clean:
	rm -rf .tox .pytest_cache .mypy_cache .direnv ardumont.pytools.egg-info ardumont.pytools-0.0.0

test:
	tox
