{
  description = "ardumont-pytools flake";

  inputs = {
    nixpkgs = {
      type = "github";
      owner = "NixOS";
      repo = "nixpkgs";
      ref = "master";
    };

    flake-utils = {
      type = "github";
      owner = "numtide";
      repo = "flake-utils";
      ref = "main";
    };
  };

  outputs = { self, nix, nixpkgs, flake-utils, ... }:
    flake-utils.lib.eachDefaultSystem
      (system:
        let lib = nixpkgs.lib;
            pkgs = nixpkgs.legacyPackages.${system};
            python3-pkgs = pkgs.python3Packages;
        in rec {
          packages."${system}" = rec {
            prometheus-client = python3-pkgs.buildPythonPackage rec {
              pname = "prometheus_client";
              version = "0.18.0";

              src = python3-pkgs.fetchPypi {
                inherit pname version;
                sha256 = "05sffrm8ijjwy9bbzh61fkfchnql70nyk656lmybpqir471aixrm";
              };

              doCheck = false;

              meta = {
                description = "The official Python 2 and 3 client for Prometheus.";
                homepage = https://github.com/prometheus/client_python/;
                license = lib.licenses.asl20;
                maintainers = with lib.maintainers; [ ardumont ];
              };
            };

            python-tvmaze = python3-pkgs.buildPythonPackage rec {
              pname = "python-tvmaze";
              version = "45c03cc2029de14870fa9c2a2f4e5828eec71423";

              src = pkgs.fetchgit {
                url = "https://github.com/ardumont/python-tvmaze";
                rev = version;
                sha256 = "1q0w5ndcmqsss7rla327vv1jcn9395x5qw5xv1zfk8c5dbwnjcfj";
              };

              doCheck = false;  # Tests are actually failing as they ping the real api
                                # (afaict)

              propagatedBuildInputs = with python3-pkgs; [ requests pytest six ];

              meta = {
                description = "Python wrapper for the TvMaze API.";
                homepage = https://github.com/yakupadakli/python-tvmaze;
                license = lib.licenses.mit;
                maintainers = with lib.maintainers; [ ardumont ];
              };
            };
            dramatiq = python3-pkgs.buildPythonPackage rec {
              pname = "dramatiq";
              version = "1.15.0";

              src = python3-pkgs.fetchPypi {
                inherit pname version;
                sha256 = "1042hsds9gw91f0v0j5a273zvwdjksllz0kw4rrsb05rg8yyj85j";
              };

              propagatedBuildInputs = with pkgs; [
                prometheus-client redis python3-pkgs.watchdog
              ];

              doCheck = false;

              meta = {
                description = "A fast and reliable distributed task processing library for Python 3.";
                homepage = https://github.com/Bogdanp/dramatiq;
                license = lib.licenses.lgpl3;
                maintainers = with lib.maintainers; [ ardumont ];
              };
            };

            pyexifinfo = python3-pkgs.buildPythonPackage rec {
              pname = "pyexifinfo";
              version = "0.4.0";

              src = python3-pkgs.fetchPypi {
                inherit pname version;
                sha256 = "1jdxskyz2vwsiha8dm7pcg59srzcybwqh9dnwsxpgzlkqnrk92sp";
              };

              propagatedBuildInputs = [ pkgs.exiftool ];

              # patch setup.py so that the embedded check on exiftool in setup.py passes
              postUnpack = with pkgs; ''
              sed -e 's,"exiftool","${exiftool}/bin/exiftool",g' -i pyexifinfo-${version}/setup.py
            '';

              meta = {
                homepage = https://github.com/guinslym/pyexifinfo;
                description = "Yet Another python wrapper for Phil Harvey's Exiftool";
                license = lib.licenses.gpl2;
                maintainers = with lib.maintainers; [ ardumont ];
              };
            };

            ardumont-pytools = python3-pkgs.buildPythonPackage rec {
              name = "ardumont.pytools";
              version = "0.0.${lib.substring 0 8 self.lastModifiedDate}.${self.shortRev or "dirty"}";

              src = ./.;

              doCheck = true;

              checkInputs = with python3-pkgs; [
                pytest pytest-mock pika
              ];

              buildInputs = with python3-pkgs; [
                setuptools-scm-git-archive python3-pkgs.pip
              ];

              propagatedBuildInputs = with python3-pkgs; [
                dramatiq click redis pyexifinfo
                pyinotify exifread python-dateutil
                arrow pyaml mutagen
                pkgs.inotify-tools # pkgs.libav
                python-tvmaze guessit attrs hachoir
                babelfish hachoir
              ];

              meta = {
                description = "Basic python tools";
                homepage = https://gitlab.com/ardumont/ardumont-pytools;
                license = lib.licenses.gpl2;
                maintainers = with lib.maintainers; [ ardumont ];
              };
            };
          };

          defaultPackage = packages."${system}".ardumont-pytools;
        });
}
