#!/usr/bin/env python3

import logging
from os import path

from setuptools import find_packages, setup

logger = logging.getLogger(__name__)


def parse_requirements(name=None):
    if name:
        reqf = "requirements-%s.txt" % name
    else:
        reqf = "requirements.txt"

    requirements = []
    if not path.exists(reqf):
        logger.warning("%s does not exist, stopping.", reqf)
        return requirements

    with open(reqf) as f:
        for line in f.readlines():
            line = line.strip()
            if not line or line.startswith("#"):
                continue
            requirements.append(line)
    return requirements


setup(
    name="ardumont.pytools",
    description="Personal python tools",
    author="Antoine R. Dumont (@ardumont)",
    author_email="antoine.romain.dumont@gmail.com",
    url="https://gitlab.com/ardumont/ardumont-pytools.git",
    packages=find_packages(),
    entry_points="""
        [console_scripts]
        ardumont-pytools=ardumont.pytools:main
        [pytools.cli.subcommands]
        task=ardumont.pytools.tools:task
        watch=ardumont.pytools.tools:watch
        tvshow=ardumont.pytools.media:tvshow
        movie=ardumont.pytools.media:movie
        fs=ardumont.pytools.fs:directory
    """,
    install_requires=parse_requirements(),
    extras_require={"testing": parse_requirements("test")},
    include_package_data=True,
)
