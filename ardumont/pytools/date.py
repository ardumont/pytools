# Copyright (C) 2018-2020  The ardumont-pytools developers
# See the AUTHORS file at the top-level directory of this distribution
# License: GNU General Public License version 3, or any later version
# See top-level LICENSE file for more information

from datetime import datetime
import re
from typing import Optional, Union

from dateutil import parser

re_date = re.compile(
    r".*(?P<day>\d{4}\d{2}\d{2}).(?P<hour>\d{2}\d{2}\d{2}).*", re.DOTALL
)


def parse_date_from_filename(filename: str) -> Optional[datetime]:
    """Parse date from a given filename (with or without extension).

    >>> parse_date_from_filename("IMG_19701020_194812")
    datetime.datetime(1970, 10, 20, 19, 48, 12)
    >>> parse_date_from_filename("VID_19701020_194812.mp4")
    datetime.datetime(1970, 10, 20, 19, 48, 12)
    >>> parse_date_from_filename("prefix-19701020_194812.mp4")
    datetime.datetime(1970, 10, 20, 19, 48, 12)

    no or too many separators so date not parse
    >>> parse_date_from_filename("VID_19701020194812.mp4")
    >>> parse_date_from_filename("VID_19701020--194812.jpg")
    >>>

    """
    res = re_date.match(filename)
    if res:
        groups = res.groupdict()
        return parser.parse(f"{groups['day']} {groups['hour']}")
    return None


def to_date(d: Optional[Union[str, datetime]]) -> Optional[datetime]:
    """Format a date extracted from metadata to datetime.

    """
    if not d:
        return None
    sdate = str(d) if not isinstance(d, str) else d
    # HACK: some other null date
    if "0000" in sdate:
        return None
    # HACK: some dates are badly formatted 'YEAR:MONTH:DAY ...'
    # and it's not recognized by parser...
    if re.match(r"[0-9]{4}:[0-9]{2}:[0-9]{2}.*", sdate):
        ddate = sdate.split(" ")
        sdate = ddate[0].replace(":", "-")
        if len(ddate) > 1:
            rest = "".join(ddate[1:])
            sdate = f"{sdate} {rest}"
    return parser.parse(sdate, ignoretz=True)


def format_date(d: Optional[Union[str, datetime]]) -> Optional[str]:
    """Format a date issued from exif metadata extraction to a formatted
    string.

    """
    if not d:
        return None
    try:
        dt = to_date(d)
        if not dt:
            return None
        return dt.strftime("%Y%m%d-%H%M%S")
    except ValueError:
        return None
