# Copyright (C) 2018-2020  The SWH & ardumont-pytools developers
# See the AUTHORS file at the top-level directory of this distribution
# License: GNU General Public License version 3, or any later version
# See top-level LICENSE file for more information

import logging
import os
from typing import Any, Mapping

from ardumont.pytools.config import load_config

logger = logging.getLogger(__name__)


CONF_ENV_VAR = "ARDUMONT_CONFIG_FILENAME"


def load_config_from_env() -> Mapping[str, Any]:
    """Load configuration from environment variable

    """
    CONFIG_NAME = os.environ.get(CONF_ENV_VAR)
    if not CONFIG_NAME:
        raise ValueError(f"Set {CONF_ENV_VAR} to set the configuration file")

    # load the config using the ARDUMONT_CONFIG_FILENAME environment variable.

    # A required top-level `dramatiq` key with the specific broker config
    # keys: type (redis, stub), host, port, queues (list of queue names)
    config = load_config(CONFIG_NAME)

    if not config.get("dramatiq"):
        raise ValueError(
            "Config error: top-level 'dramatiq' key is missing" f" in {CONFIG_NAME}"
        )

    return config


def define_broker(config: Mapping[str, Any]):
    """Define the broker according to config and set it as global broker

    Raises
        ValueError in case of an unsupported broker is configured

    Returns
       Union[RedisBroker, StubBroker]

    """
    logger.debug(f"Define broker with config {config}")
    conf_broker = config["dramatiq"]["broker"]
    broker_type = conf_broker["type"]
    if conf_broker["type"] == "redis":
        from dramatiq.brokers.redis import RedisBroker

        host = conf_broker["host"]
        port = int(conf_broker["port"])
        # Force the broker to be redis instead of the default rabbit
        broker = RedisBroker(host=host, port=port)
    elif broker_type == "stub":
        from dramatiq.brokers.stub import StubBroker

        broker = StubBroker()
    else:
        raise ValueError(f"Unsupported {broker_type}, only redis or stub!")
    for queue_name in conf_broker.get("queues", {}):
        broker.declare_queue(queue_name)
    from dramatiq import set_broker

    set_broker(broker)
    return broker


def load_broker_from_env():
    """Load dramatiq setup from environment

    """
    return define_broker(load_config_from_env())
