# Copyright (C) 2020 The SWH & ardumont-pytools developers
# See the AUTHORS file at the top-level directory of this distribution
# License: GNU General Public License version 3, or any later version
# See top-level LICENSE file for more information

import os

from ardumont.pytools.dramatiq import load_broker_from_env

# HACK: to avoid dramatiq's state expectation
if os.environ.get("ARDUMONT_CONFIG_FILENAME"):
    load_broker_from_env()
