# Copyright (C) 2019-2020  The ardumont-pytools developers
# See the AUTHORS file at the top-level directory of this distribution
# License: GNU General Public License version 3, or any later version
# See top-level LICENSE file for more information

from io import StringIO
from os import path
from typing import Iterable, Optional, Tuple

from click.testing import CliRunner

from ardumont.pytools.fs import directory, read_from_stdin


def test_read_from_stdin(monkeypatch, prepare_fs_from_manifest):
    manifest, fake_slash = prepare_fs_from_manifest
    _, data = read_manifest(manifest, fake_slash)
    monkeypatch.setattr("ardumont.pytools.fs.stdin", StringIO(data))

    filepaths = list(read_from_stdin())
    assert len(filepaths) > 0

    for filepath in filepaths:
        assert filepath
        assert not filepath.startswith("./.")
        assert not filepath.startswith("./") and not filepath.startswith(".")
        assert path.exists(filepath)


def read_manifest(
    manifest: str, fake_slash: str, with_dir: Optional[bool] = True
) -> Tuple[Iterable[str], str]:
    """Read the manifest of files and returns the same manifest with
       full path computed.

    """
    # prepare
    with open(manifest, "r") as f:
        lines = []
        for line in f.readlines():
            line = line.rstrip("\n")
            if with_dir:
                line = path.join(fake_slash, path.dirname(line))
            else:
                line = path.join(fake_slash, line)
            lines.append(line)

        return lines, "\n".join(lines)


def test_rename_directory_audio(monkeypatch, prepare_fs_from_manifest):
    manifest, fake_slash = prepare_fs_from_manifest

    # prepare
    dirpaths, data = read_manifest(manifest, fake_slash)
    monkeypatch.setattr("ardumont.pytools.fs.stdin", StringIO(data))

    # when
    runner = CliRunner()

    result = runner.invoke(directory, ["audio", "--help"])

    assert result.exit_code == 0, f"output: {result.output}"

    # then (did nothing)
    for dirpath in dirpaths:
        assert path.exists(dirpath)

    assert directory is not None
    result = runner.invoke(directory, ["audio"])
    assert result.exit_code == 0, f"output: {result.output}"

    # then (did rename matching directories)
    for dirpath in dirpaths:
        if "." in dirpath:
            assert not path.exists(dirpath)
            new_dirpath = dirpath.replace(".", "-")
            assert path.exists(new_dirpath)
        else:
            assert path.exists(dirpath)
