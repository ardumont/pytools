# Copyright (C) 2018-2020  The ardumont-pytools developers
# See the AUTHORS file at the top-level directory of this distribution
# License: GNU General Public License version 3, or any later version
# See top-level LICENSE file for more information

import pytest

from ardumont.pytools.config import exists_accessible, load_config


def test_load_config(ardumont_config, ardumont_config_yaml):
    actual_config = load_config(ardumont_config)
    assert actual_config == ardumont_config_yaml


def test_exist_accessible(ardumont_config):
    assert exists_accessible(ardumont_config)


def test_exist_accessible_failure():
    assert not exists_accessible("/tmp/inexistent-file")
    assert not exists_accessible("/tmp/something/inexistent-file")


def test_load_config_do_not_exist(ardumont_config):
    path = ardumont_config + "/tmp"
    with pytest.raises(NotADirectoryError):
        load_config(path)
