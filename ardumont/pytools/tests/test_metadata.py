# Copyright (C) 2019-2020  The ardumont-pytools developers
# See the AUTHORS file at the top-level directory of this distribution
# License: GNU General Public License version 3, or any later version
# See top-level LICENSE file for more information

import datetime
from os.path import exists, join
import shutil

from mutagen.mp4 import MP4
import pytest

from ardumont.pytools.date import to_date
from ardumont.pytools.metadata import (
    _extract_date,
    clean,
    date_file,
    date_from_metadata,
    extract_date,
    extract_metadata,
)


def test_extract_metadata_ok(datadir):
    filepath = join(datadir, "image.jpg")
    assert exists(filepath)

    meta = extract_metadata(filepath)

    assert meta
    assert isinstance(meta, dict)


def test_extract_metadata_no_result():
    filepath = "something-inexistent"
    assert not exists(filepath)
    meta = extract_metadata(filepath)

    assert meta == {}


def test__extract_date(datadir):
    filepath = join(datadir, "image.jpg")
    assert exists(filepath)

    meta = extract_metadata(filepath)

    fields = ["File:FileAccessDate", "File:FileInodeChangeDate", "File:FileModifyDate"]

    dates = []
    for field in fields:
        assert field in meta.keys()
        dates.append(to_date(meta[field]))

    actual_date = _extract_date(filepath, fields=fields)
    assert actual_date == min(dates)

    actual_date2 = _extract_date(meta, fields=fields)
    assert actual_date2 == min(dates)


def test__extract_date_no_result(datadir):
    # filepath does not exist
    filepath = "something-inexistent"
    assert not exists(filepath)
    assert _extract_date(filepath, []) is None

    # filepath exists but no field passed as parameter
    filepath = join(datadir, "image.jpg")
    assert exists(filepath)

    actual_date = _extract_date(filepath, fields=[])
    assert actual_date is None

    # no relevant fields in result

    meta = extract_metadata(filepath)
    fields = ["File:FileAccessDate", "File:FileInodeChangeDate", "File:FileModifyDate"]

    for field in fields:
        meta.pop(field)  # remove fields with relevant data

    # then nothing returns
    actual_date3 = _extract_date(meta, fields=fields)
    assert actual_date3 is None


def test_date_file(datadir):
    filepath = join(datadir, "image.jpg")
    assert exists(filepath)

    meta = extract_metadata(filepath)
    fields = ["File:FileAccessDate", "File:FileModifyDate"]
    dates = []
    for field in fields:
        assert field in meta.keys()
        dates.append(to_date(meta[field]))

    assert date_file(filepath) == min(dates)
    assert date_file(meta) == min(dates)


def test_date_from_metadata(datadir):
    filepath = join(datadir, "image.jpg")
    assert exists(filepath)

    meta = extract_metadata(filepath)
    fields = ["File:FileAccessDate", "File:FileModifyDate"]
    dates = []
    for field in fields:
        assert field in meta.keys()
        dates.append(to_date(meta[field]))

    assert date_from_metadata(filepath) == min(dates)
    assert date_from_metadata(meta) == min(dates)


def test_extract_date_from_metadata(datadir):
    """Date extracted from file metadata"""
    filepath = join(datadir, "image.jpg")
    assert exists(filepath)
    expected_date = date_file(filepath)

    # when
    actual_date = extract_date(filepath)

    assert actual_date == expected_date
    assert isinstance(actual_date, datetime.datetime)


@pytest.fixture
def mock_date_from_metadata(mocker):
    date_from_metadata = mocker.patch("ardumont.pytools.metadata.date_from_metadata")
    date_from_metadata.return_value = {}
    return date_from_metadata


@pytest.mark.usefixtures("mock_date_from_metadata")
def test_extract_date_no_metadata_date_from_filename(datadir):
    """No actual_date from metadata nor filename """
    filepath = join(datadir, "IMG_image-no-metadata-20150515_000000.jpg")
    actual_date = extract_date(filepath)
    assert isinstance(actual_date, datetime.datetime)
    assert actual_date == datetime.datetime(2015, 5, 15, 0, 0, 0)


@pytest.mark.usefixtures("mock_date_from_metadata")
def test_extract_date_no_metadata_nor_filename(datadir):
    """No actual_date from metadata nor filename """
    filepath = join(datadir, "IMG_image-no-metadata.jpg")
    actual_date = extract_date(filepath)
    assert actual_date is None


def test_clean_on_no_video(datadir):
    """Does nothing as it's not a video"""
    filepath = join(datadir, "image.jpg")
    assert exists(filepath)
    actual_path = clean(filepath)
    assert actual_path == filepath


def test_clean_video_raise(datadir, tmp_path):
    """Does nothing as it's not a video"""
    video_name = "not-a-video.mp4"
    filepath = join(datadir, video_name)
    assert exists(filepath)
    videopath = str(tmp_path / video_name)
    shutil.copy(filepath, videopath)

    # meta = MP4(videopath)
    # assert meta

    assert exists(videopath)

    with pytest.raises(Exception, match="not a MP4"):
        clean(videopath)


def test_clean_video(datadir, tmp_path):
    """Does nothing as it's not a video"""
    video_name = "video.mp4"
    filepath = join(datadir, video_name)
    assert exists(filepath)
    videopath = str(tmp_path / video_name)
    shutil.copy(filepath, videopath)

    meta = MP4(videopath)
    assert meta
    assert exists(videopath)

    actual_path = clean(videopath)
    assert actual_path == videopath

    # it's been cleaned now
    meta = MP4(videopath)
    assert meta == {}
