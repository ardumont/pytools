# Copyright (C) 2019-2021  The ardumont-pytools developers
# See the AUTHORS file at the top-level directory of this distribution
# License: GNU General Public License version 3, or any later version
# See top-level LICENSE file for more information

from collections import OrderedDict
import hashlib
import logging
import os
from os.path import basename, exists, join, splitext
from shutil import copyfile

import pytest

from ardumont.pytools.utils import (
    _movie_rename_cmds,
    _tvshow_rename_cmds,
    check_for_hash_clash,
    compute_destination_path,
    copy_or_rename_with_clash,
    is_video,
    movie_rename,
    pre_sanitize_string,
    read_tvshow_id,
    rename,
    sanitize,
    sanitize_string,
    sha512_path,
    tvshow_rename,
    yaml_export,
)

logger = logging.getLogger(__name__)


def test_compute_destination_path(stub_broker):
    for src_path, expected_destination_path in [
        (
            "/some/path/IMG_19701020_194812.jpg",
            "/volume/pix/1970/10/19701020-194812.jpg",
        ),
        (
            "/some/path/VID_19801224_204812.mp4",
            "/volume/pix/1980/12/19801224-204812.mp4",
        ),
        ("/some/no-date.mp4", None),
    ]:
        actual_dst_path = compute_destination_path(src_path, "/volume/pix")
        assert actual_dst_path == expected_destination_path


def test_is_video():
    extensions_to_check = ["mp4", "avi", "MKV"]
    for ext in ["mp4", "avi"]:
        path = f"something.{ext}"
        assert is_video(path, extensions_to_check)
        path = f"SOMETHING.{ext.upper()}"
        assert is_video(path, extensions_to_check)

    for ext in ["txt", "str"]:
        path = f"something.{ext}"
        assert not is_video(path, extensions_to_check)


def touch(path, content=""):
    if not exists(path):
        with open(path, "w") as f:
            f.write(content)
    return path


def test_rename(tmp_path):
    folder = tmp_path / "something"
    os.mkdir(str(folder))
    oldpath = str(folder / "old.txt")
    newpath_name = "new.txt"
    newpath = str(folder / newpath_name)

    touch(oldpath)
    assert exists(oldpath)
    assert not exists(newpath)

    actual_result = rename(oldpath, newpath)
    assert actual_result == newpath

    assert not exists(oldpath)
    assert exists(newpath)

    newname, ext = splitext(newpath_name)
    for i in range(1, 20):
        touch(oldpath)
        actual_result = rename(oldpath, newpath)
        idx = "%02d" % i
        assert actual_result == f"{folder}/{newname}.{idx}{ext}"
        assert exists(newpath)
        assert not exists(oldpath)


def test_sanitize_string():
    for s, expected_sanitized_s in [
        ("With-dash-parameter", "with-dash-parameter"),
        ("with_Mixed Term-ok ko-ext", "with-mixed-term-ok-ko-ext"),
        ("Nothingtochange", "nothingtochange"),
        ("blah  blah  Blah", "blah-blah-blah"),
    ]:
        assert sanitize_string(s) == expected_sanitized_s


def test_sanitize_raise():
    """Bad filename pattern should raise"""
    with pytest.raises(AssertionError, match="should have a ."):
        sanitize("/path/to/with-daSh2015parameter-no-extension")

    with pytest.raises(
        ValueError, match="should have either a 'year' or a 'season' pattern"
    ):
        sanitize("/path/path-no-date.ext")

    with pytest.raises(
        ValueError, match="should have either a 'year' or a 'season' pattern"
    ):
        sanitize("/path/path-no-date-e100.ext")


def test_pre_sanitize_string():
    for input, output in [
        ("[[(blah}]]", "blah"),
        (
            "/path/[another-way]1982-noise-1080p.mp4",
            "/path/another-way1982-noise-1080p.mp4",
        ),
        (
            "/path/another-way-(1950)-noise-1080p.mp4",
            "/path/another-way-1950-noise-1080p.mp4",
        ),
    ]:
        assert pre_sanitize_string(input) == output


def test_sanitize():
    """Good filename pattern should be renamed appropriately"""
    for path, expected_sanitized_path in [
        # movies
        ("/path/to/with-daSh2015 noise.ext", "/path/to/with-dash-2015.ext"),
        ("/with_mixed term-ok ko2012.ext", "/with-mixed-term-ok-ko-2012.ext"),
        (
            "/a/b/c/NOTHINGTOCHANGEbutcase-2019.ext",
            "/a/b/c/nothingtochangebutcase-2019.ext",
        ),
        (
            "/e/f/something witH multiple ext.2001.jpg",
            "/e/f/something-with-multiple-ext-2001.jpg",
        ),
        (
            "/path/the.Way.Of.The.mind.1982.noisy.noise.noise.mp4",
            "/path/the-way-of-the-mind-1982.mp4",
        ),
        ("/path/another-Way1982-noise-1080p.mp4", "/path/another-way-1982.mp4"),
        ("/path/[another-way]1982-noise-1080p.mp4", "/path/another-way-1982.mp4"),
        ("/path/another-way [2010]-noise-1080p.mp4", "/path/another-way-2010.mp4"),
        ("/path/another-way (1950)-noise-1080p.mp4", "/path/another-way-1950.mp4"),
        # shows
        ("/path/show-2017-S6E66-noise-noise.mp4", "/path/show-2017-s6e66.mp4"),
        (
            "/path/yet-another-Ways01e10-noise-noise.mp4",
            "/path/yet-another-way-s01e10.mp4",
        ),
        ("/path/yet-yets10-noisy-noisy.mp4", "/path/yet-yet-s10.mp4"),
        ("/hence-s6e6-noisy-noisy.mp4", "/hence-s6e6.mp4"),
        ("/path/noisy-[s10]-noisy.mp4", "/path/noisy-s10.mp4"),
        ("/path/noisy-(s05)-noisy.mp4", "/path/noisy-s05.mp4"),
        ("/path/[something] yet-s1-noisy-noisy.mp4", "/path/something-yet-s1.mp4"),
    ]:
        actual_path = sanitize(path)
        assert actual_path == expected_sanitized_path


def test_clash_with_clash(tmp_path):
    path = str(tmp_path / "test")
    with open(path, "w") as f:
        f.write("something")

    dir = str(tmp_path / "a")
    os.mkdir(dir)
    another_path = str(tmp_path / "a" / "test")
    with open(another_path, "w") as f:
        f.write("something")

    assert check_for_hash_clash(path, path)
    assert check_for_hash_clash(path, another_path)
    assert check_for_hash_clash(another_path, another_path)


def test_clash_ok(tmp_path):
    path0 = str(tmp_path / "test")
    with open(path0, "w") as f:
        f.write("something")

    path1 = str(tmp_path / "test2")
    with open(path1, "w") as f:
        f.write("sthg different")

    assert not check_for_hash_clash(path1, path0)
    assert not check_for_hash_clash(path0, path1)


def test_sha512_path(tmp_path):
    for data in ["something", "else", "foobar"]:
        expected_hash = hashlib.sha512(data.encode("utf-8")).hexdigest()
        dirpath = tmp_path / data
        dirpath.mkdir()
        path = str(tmp_path / data / "test")
        with open(path, "w") as f:
            f.write(data)

        actual_hash = sha512_path(path)
        assert actual_hash == expected_hash


def test_tvshow_rename(mocker):
    mock_rename = mocker.patch("ardumont.pytools.utils._tvshow_rename_cmds")
    mock_rename.return_value = ["echo ok"]
    actual_result = tvshow_rename("some-path")
    # assert mock_rename.assert_called_once_with('some-path', {})
    assert actual_result


def test_tvshow_rename_fail(mocker):
    mock_rename = mocker.patch("ardumont.pytools.utils._tvshow_rename_cmds")
    mock_rename.return_value = []
    actual_result = tvshow_rename("some-path", config={})
    assert not actual_result


def test_movie_rename(mocker):
    mock_rename = mocker.patch("ardumont.pytools.utils._movie_rename_cmds")
    mock_rename.return_value = ["echo ok"]
    actual_result = movie_rename("some-path")
    # assert mock_rename.assert_called_once_with('some-path', {})
    assert actual_result


def test_movie_rename_fail(mocker):
    mock_rename = mocker.patch("ardumont.pytools.utils._movie_rename_cmds")
    mock_rename.return_value = []
    actual_result = movie_rename("some-path", config={})
    assert not actual_result


def test_read_tvshow_id_file_does_not_exist():
    assert read_tvshow_id("/inexistent/file") is None


def test_read_tvshow_id_wrong_content(tmp_path):
    fake_path = tmp_path / "something"
    fake_path.mkdir()
    assert fake_path.exists()
    fake_tvtool_file = fake_path / ".tvtool"
    fake_tvtool_path = str(fake_tvtool_file)
    with open(fake_tvtool_path, "w") as f:
        f.write("not an id\nsome\nirrelevant data")
    assert fake_tvtool_file.exists()
    assert read_tvshow_id(fake_tvtool_path) is None


def test_read_tvshow_id(tmp_path):
    fake_path = tmp_path / "something"
    fake_path.mkdir()
    assert fake_path.exists()
    fake_tvtool_file = fake_path / ".tvtool"
    fake_tvtool_path = str(fake_tvtool_file)
    tvshow_id = 19
    with open(fake_tvtool_path, "w") as f:
        f.write(str(tvshow_id))
    assert fake_tvtool_file.exists()
    assert read_tvshow_id(fake_tvtool_path) == 19


@pytest.mark.parametrize("rel_path", [".", ".."])
def test__tvshow_rename_cmds(datadir, tmp_path, config_sort_media, rel_path):
    """Correct tvshow renamer command triggered"""
    cfg = config_sort_media
    app_cfg = cfg["application-command"]
    # prepare data
    fake_tvshow = tmp_path / "fake"
    fake_tvshow.mkdir()
    fake_tvshow_path = str(fake_tvshow)
    tvshow_id = 404

    tvshow_id_filename = app_cfg["tvshow-id-filename"]
    tvtool_tvshow_config_path = join(fake_tvshow_path, rel_path, tvshow_id_filename)
    with open(tvtool_tvshow_config_path, "w") as f:
        f.write(str(tvshow_id))

    # when
    actual_cmds = _tvshow_rename_cmds(fake_tvshow_path, config=cfg)

    tvtool_cmd = join(app_cfg["command"])
    tvtool_config_path = join(app_cfg["config-path"])

    expected_cmd = [
        tvtool_cmd,
        f"--config-path={tvtool_config_path}",
        f"--id-tvmaze={tvshow_id}",
        fake_tvshow_path,
    ]

    assert len(actual_cmds) == 1, f"expected cmd: [{expected_cmd}]"
    assert actual_cmds == [expected_cmd]


def test__tvshow_rename_cmds_no_tvshow_id(datadir, tmp_path, config_sort_media):
    """With no tvshow id, we cannot trigger tvtool correctly, so fail"""
    cfg = config_sort_media
    app_cfg = cfg["application-command"]
    # prepare data
    fake_tvshow = tmp_path / "fake"
    fake_tvshow.mkdir()
    fake_tvshow_path = str(fake_tvshow)

    # when
    actual_cmds = _tvshow_rename_cmds(fake_tvshow_path, config=cfg)

    tvtool_cmd = join(app_cfg["command"])
    tvtool_config_path = join(app_cfg["config-path"])

    expected_cmd = [
        tvtool_cmd,
        f"--config-path={tvtool_config_path}",
        fake_tvshow_path,
    ]

    assert len(actual_cmds) == 1, f"expected cmd: [{expected_cmd}]"
    assert actual_cmds == [expected_cmd]


def test__movie_rename_cmds(datadir, tmp_path, config_sort_media):
    """Correct tvshow renamer command triggered"""
    cfg = config_sort_media
    app_cfg = cfg["application-command"]
    # prepare data
    fake_movie = tmp_path / "fake"
    fake_movie.mkdir()
    fake_movie_path = str(fake_movie)

    # when
    actual_cmds = _movie_rename_cmds(fake_movie_path, config=cfg)

    tvtool_cmd = join(app_cfg["command"])
    tvtool_config_path = join(app_cfg["config-path"])

    expected_cmd = [
        tvtool_cmd,
        f"--config-path={tvtool_config_path}",
        fake_movie_path,
    ]

    assert len(actual_cmds) == 1, f"expected cmd: [{expected_cmd}]"
    assert actual_cmds == [expected_cmd]


def test_yaml_export():
    data = [
        OrderedDict(
            [
                ("seriesName", "Lost in Space"),
                ("id", 72923),
                ("firstAired", "1965-9-15"),
                ("language", "en"),
                ("status", "Ended"),
                ("network", "sbc"),
            ]
        ),
        OrderedDict(
            [
                ("seriesName", "in Space Lost"),
                ("id", 343253),
                ("firstAired", "2018-4-13"),
                ("language", "en"),
                ("status", "Continuing"),
                ("network", "filx"),
            ]
        ),
        OrderedDict(
            [
                ("seriesName", "The Robinsons: Lost in Space"),
                ("id", 248282),
                ("firstAired", "2008-11-20"),
                ("language", "en"),
                ("status", "Ended"),
                ("network", "bw"),
            ]
        ),
        OrderedDict(
            [
                ("seriesName", "Lose in Space"),
                ("id", 348509),
                ("language", "en"),
                ("status", "Ended"),
            ]
        ),
    ]

    actual_yaml = yaml_export(data)

    expected_yaml = """- seriesName: Lost in Space
  id: 72923
  firstAired: 1965-9-15
  language: en
  status: Ended
  network: sbc
- seriesName: in Space Lost
  id: 343253
  firstAired: 2018-4-13
  language: en
  status: Continuing
  network: filx
- seriesName: 'The Robinsons: Lost in Space'
  id: 248282
  firstAired: '2008-11-20'
  language: en
  status: Ended
  network: bw
- seriesName: Lose in Space
  id: 348509
  language: en
  status: Ended
"""
    assert actual_yaml == expected_yaml


def test_copy_or_rename_with_clash(prepare_fs_from_manifest):
    manifest, fake_slash = prepare_fs_from_manifest
    dst_rootpath = join(fake_slash, "volume/something")
    assert exists(dst_rootpath) is False

    with open(manifest, "r") as f:
        lst = f.readlines()
        for path in lst:
            src_path = join(fake_slash, path.rstrip())

            src_name = basename(src_path)
            dst_path = join(dst_rootpath, src_name)

            assert exists(src_path) is True
            assert exists(dst_path) is False

            # no clash, plain copy
            copy_or_rename_with_clash(src_path, dst_path, cleanup=False)

            assert exists(dst_rootpath) is True  # got created
            assert exists(src_path) is True
            assert exists(dst_path) is True

            # remove it for next run
            os.unlink(dst_path)

            # no clash, plain renaming
            copy_or_rename_with_clash(src_path, dst_path, cleanup=True)

            assert exists(src_path) is False  # got renamed
            assert exists(dst_path) is True

            # copy back the source from the destination for next run
            copyfile(dst_path, src_path)
            assert exists(src_path) is True
            assert exists(dst_path) is True

            # clash, do nothing
            copy_or_rename_with_clash(src_path, dst_path, cleanup=False)

            assert exists(src_path) is True  # not touched
            assert exists(dst_path) is True  # already existing so nothing

            # clash, rename
            copy_or_rename_with_clash(src_path, dst_path, cleanup=True)

            assert exists(src_path) is False  # renamed
            assert exists(dst_path) is True

    # created along the way
    assert exists(dst_rootpath) is True

    copyfile(dst_path, src_path)
    # create a file with different hash so the clash exists but we want
    # to keep the new file
    with open(dst_path, "w") as f:
        f.write("something completely different ")
        f.write("not really relevant here, just to ")
        f.write("make the file have a different hash")

    # clash, rename
    copy_or_rename_with_clash(src_path, dst_path, cleanup=False)

    dest_path_prefix, dest_path_ext = dst_path.split(".")
    dst_path_1 = f"{dest_path_prefix}-01.{dest_path_ext}"
    assert exists(src_path) is True
    assert exists(dst_path) is True
    assert exists(dst_path_1) is True

    with open(dst_path_1, "w") as f:
        f.write("another content")

    # clash, remove the src
    copy_or_rename_with_clash(src_path, dst_path, cleanup=True)

    dst_path_2 = f"{dest_path_prefix}-02.{dest_path_ext}"
    assert exists(src_path) is False
    assert exists(dst_path) is True
    assert exists(dst_path_1) is True
    assert exists(dst_path_2) is True
