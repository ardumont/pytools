# Copyright (C) 2019-2021  The ardumont-pytools developers
# See the AUTHORS file at the top-level directory of this distribution
# License: GNU General Public License version 3, or any later version
# See top-level LICENSE file for more information

from io import StringIO
import json
import os

import pytest

from ardumont.pytools.media import movie as cli_movie
from ardumont.pytools.media import tvshow as cli_tvshow
from ardumont.pytools.utils import sanitize, sanitize_string

from .test_fs import read_manifest


@pytest.mark.usefixtures("mock_search_tvshow")
def test_tvshow_search(cli_runner):
    """Looking up existing tvshow should output formatted result"""
    # one of the fake tvshow
    result = cli_runner.invoke(cli_tvshow, ["search", "ToNyX"])

    assert result.exit_code == 0, result.output

    assert result.output == "name: ToNyX\ntvmaze_id: 0\n\n"


def test_tvshow_init(cli_runner, tmp_path):
    """Initialize new tvshow should prepare the fs accordingly"""
    # one of the fake tvshow

    tvshow = "something"
    tvmaze_id = 123
    rootpath = str(tmp_path)

    expected_rootpath = f"{rootpath}/{tvshow[0]}/{tvshow}"
    assert not os.path.exists(expected_rootpath)
    expected_tvtool = f"{expected_rootpath}/.tvtool"
    assert not os.path.exists(expected_tvtool)

    result = cli_runner.invoke(
        cli_tvshow, ["init", tvshow, "--tvmaze-id", tvmaze_id, "--root-path", rootpath]
    )

    assert result.exit_code == 0, result.output

    expected_rootpath = f"{rootpath}/{tvshow[0]}/{tvshow}"
    assert os.path.exists(expected_rootpath)
    expected_tvtool = f"{expected_rootpath}/.tvshow-id"
    assert os.path.exists(expected_tvtool)

    with open(expected_tvtool, "r") as f:
        tvmaze_id = f.read()

    assert tvmaze_id == str(tvmaze_id)


def test_tvshow_init_conflict_ignore(cli_runner, tmp_path):
    """Initialize tvshow when setup exist with same data does nothing"""
    tvshow = "same-result"
    tvmaze_id = 456
    rootpath = str(tmp_path)

    expected_rootpath = f"{rootpath}/{tvshow[0]}/{tvshow}"
    assert not os.path.exists(expected_rootpath)
    expected_tvtool = f"{expected_rootpath}/.tvshow-id"
    assert not os.path.exists(expected_tvtool)

    os.makedirs(expected_rootpath)

    # Prepare existing file
    with open(expected_tvtool, "w") as f:
        f.write(str(tvmaze_id))

    result = cli_runner.invoke(
        cli_tvshow, ["init", tvshow, "--tvmaze-id", tvmaze_id, "--root-path", rootpath]
    )

    assert result.exit_code == 0, result.output

    with open(expected_tvtool, "r") as f:
        file_tvmaze_id = f.read()
        assert int(file_tvmaze_id) == tvmaze_id


def test_tvshow_init_conflict_warning(cli_runner, tmp_path):
    """Initialize new tvshow when something exists should raise"""
    tvshow = "conflict"
    tvmaze_id = 789
    rootpath = str(tmp_path)

    expected_rootpath = f"{rootpath}/{tvshow[0]}/{tvshow}"
    assert not os.path.exists(expected_rootpath)
    expected_tvtool = f"{expected_rootpath}/.tvshow-id"
    assert not os.path.exists(expected_tvtool)

    os.makedirs(expected_rootpath)

    with open(expected_tvtool, "w") as f:
        f.write(str(tvmaze_id))

    result = cli_runner.invoke(
        cli_tvshow, ["init", tvshow, "--tvmaze-id", 123, "--root-path", rootpath]
    )

    assert result.exit_code == 1, result.output
    exc = result.exception
    assert isinstance(exc, ValueError)
    assert "Conflict: Existing entry" in exc.args[0]

    # Nothing changed
    with open(expected_tvtool, "r") as f:
        previous_tvmaze_id = f.read(tvmaze_id)
        assert int(previous_tvmaze_id) == tvmaze_id


def test_tvshow_init_conflict_force(cli_runner, tmp_path):
    """Initialize tvshow with force when something exists should overwrite"""
    tvshow = "overwrite"
    tvmaze_id = 789
    rootpath = str(tmp_path)

    expected_rootpath = f"{rootpath}/{tvshow[0]}/{tvshow}"
    assert not os.path.exists(expected_rootpath)
    expected_tvtool = f"{expected_rootpath}/.tvshow-id"
    assert not os.path.exists(expected_tvtool)

    os.makedirs(expected_rootpath)

    with open(expected_tvtool, "w") as f:
        f.write(str(tvmaze_id))

    new_tvmaze_id = 123
    result = cli_runner.invoke(
        cli_tvshow,
        [
            "init",
            tvshow,
            "--tvmaze-id",
            new_tvmaze_id,
            "--root-path",
            rootpath,
            "--force",
        ],
    )

    assert result.exit_code == 0, result.output

    # Nothing changed
    with open(expected_tvtool, "r") as f:
        file_tvmaze_id = f.read(tvmaze_id)
        assert int(file_tvmaze_id) == new_tvmaze_id


def test_tvshow_update_new_entry_out_of_nothing(cli_runner):
    """tvshow media entry from no media.json"""
    tvshow = "New New Tvshow"
    tvmaze_id = 123

    result = cli_runner.invoke(
        cli_tvshow,
        [
            "update",
            tvshow,
            "--tvmaze-id",
            tvmaze_id,
        ],
    )

    assert result.exit_code == 0, result.output
    new_json = json.loads(result.output)

    assert new_json == {
        tvshow: {"short": sanitize_string(tvshow), "tvmaze-id": tvmaze_id}
    }


def test_tvshow_update_new_entry(cli_runner, datadir):
    """Update tvshow media entry"""
    media_json = os.path.join(datadir, "media.json")

    tvshow = "New New Tvshow"
    tvmaze_id = 123

    result = cli_runner.invoke(
        cli_tvshow,
        ["update", tvshow, "--tvmaze-id", tvmaze_id, "--media-json", media_json],
    )

    assert result.exit_code == 0, result.output
    new_json = json.loads(result.output)

    entry = new_json[tvshow]

    assert entry == {"short": sanitize_string(tvshow), "tvmaze-id": tvmaze_id}


def test_tvshow_update_conflict_nothing(cli_runner, datadir):
    """Update tvshow media entry with something identical, do nothing"""
    media_json = os.path.join(datadir, "media.json")
    existing_entries = json.loads(open(media_json, "r").read())

    tvshow = "TV show 1"
    existing_tvshow_tvmaze_id = existing_entries[tvshow]["tvmaze-id"]

    result = cli_runner.invoke(
        cli_tvshow,
        [
            "update",
            tvshow,
            "--tvmaze-id",
            existing_tvshow_tvmaze_id,  # id diverge, real conflict
            "--media-json",
            media_json,
        ],
    )

    assert result.exit_code == 0, result.output

    # nothing changed
    new_json = json.loads(result.output)
    assert len(new_json) == len(existing_entries)
    assert new_json == existing_entries


def test_tvshow_update_conflict_raise(cli_runner, datadir):
    """Update tvshow media entry"""
    media_json = os.path.join(datadir, "media.json")
    existing_entries = json.loads(open(media_json, "r").read())

    tvshow = "TV show 1"
    existing_tvshow_tvmaze_id = existing_entries[tvshow]["tvmaze-id"]

    result = cli_runner.invoke(
        cli_tvshow,
        [
            "update",
            tvshow,
            "--tvmaze-id",
            existing_tvshow_tvmaze_id + 10,  # id diverge, real conflict
            "--media-json",
            media_json,
        ],
    )

    assert result.exit_code == 1, result.output

    exc = result.exception
    assert isinstance(exc, ValueError)
    assert exc.args == (
        "Conflict: Existing entry %s for %s",
        tvshow,
        existing_entries[tvshow],
    )


def test_tvshow_update_conflict_overwrite(cli_runner, datadir):
    """Update tvshow media entry"""
    media_json = os.path.join(datadir, "media.json")
    existing_entries = json.loads(open(media_json, "r").read())
    tvshow = "TV show 1"

    new_tdvb_id = existing_entries[tvshow]["tvmaze-id"] + 100
    result = cli_runner.invoke(
        cli_tvshow,
        [
            "update",
            tvshow,
            "--tvmaze-id",
            new_tdvb_id,
            "--media-json",
            media_json,
            "--force",
        ],
    )

    assert result.exit_code == 0, result.output

    new_json = json.loads(result.output)
    assert len(new_json) == len(existing_entries)

    assert new_json[tvshow] == {
        "short": existing_entries[tvshow]["short"],
        "tvmaze-id": new_tdvb_id,
    }


def test_tvshow_init_from_json_failure(cli_runner):
    """No media json at all should raise"""
    result = cli_runner.invoke(cli_tvshow, ["init-from-json"])
    assert result.exit_code == 1, result.output
    exc = result.exception
    assert isinstance(exc, ValueError)
    assert "Please provide a media.json file" in exc.args[0]


def test_tvshow_init_from_json_invalid_entries(cli_runner, datadir):
    """Invalid entries in media json should be in output"""
    media_json = os.path.join(datadir, "media-invalid.json")

    result = cli_runner.invoke(
        cli_tvshow, ["init-from-json", "--media-json", str(media_json)]
    )
    assert "Invalid entries" in result.output
    assert result.exit_code == 0, result.output  # still ok on other entries


def test_tvshow_init_from_json(cli_runner, datadir, tmp_path):
    """Invalid entries in media json should be output"""
    media_json = os.path.join(datadir, "media.json")

    media = json.loads(open(media_json).read())

    rootpath = str(tmp_path)
    assert len(media.items()) > 0

    for title, entry in media.items():
        tvshow = entry["short"]
        expected_rootpath = f"{rootpath}/{tvshow[0]}/{tvshow}"
        assert not os.path.exists(expected_rootpath)
        expected_tvtool = f"{expected_rootpath}/.tvshow-id"
        assert not os.path.exists(expected_tvtool)

    result = cli_runner.invoke(
        cli_tvshow,
        [
            "init-from-json",
            "--media-json",
            str(media_json),
            "--root-path",
            rootpath,
        ],
    )
    assert result.exit_code == 0, result.output

    for title, entry in media.items():
        tvshow = entry["short"]
        expected_rootpath = f"{rootpath}/{tvshow[0]}/{tvshow}"
        assert os.path.exists(expected_rootpath)
        expected_tvtool = f"{expected_rootpath}/.tvshow-id"
        assert os.path.exists(expected_tvtool)


def test_movie_sanitize(cli_runner, datadir, tmp_path):
    """Sanitize video path should work"""
    tmp_video = tmp_path / "test2010-noisy.mp4"
    with open(tmp_video, "w") as f:
        f.write("")

    video_path = str(tmp_video)
    assert os.path.exists(video_path)
    new_video_path = sanitize(tmp_video)
    assert not os.path.exists(new_video_path)

    result = cli_runner.invoke(cli_movie, ["sanitize", video_path])

    assert result.exit_code == 0, result.output
    assert not os.path.exists(video_path)
    assert os.path.exists(new_video_path) is True


def test_cli_movie_sort(cli_runner, monkeypatch, prepare_fs_with_movies):
    manifest, fake_slash = prepare_fs_with_movies

    filepaths, data = read_manifest(manifest, fake_slash, with_dir=False)
    monkeypatch.setattr("ardumont.pytools.fs.stdin", StringIO(data))

    new_filepaths = {}
    for filepath in filepaths:
        filename = os.path.basename(filepath)
        dirname = os.path.dirname(filepath)
        assert "." in filename
        name = os.path.splitext(filename)
        assert "-" in filename
        year = name[0].split("-")[-1]
        iyear = int(year)
        assert isinstance(iyear, int)
        folder_parent_year = str(iyear - (iyear % 10))
        new_path = os.path.join(dirname, folder_parent_year, filename)
        new_filepaths[filepath] = new_path

    # when
    result = cli_runner.invoke(cli_movie, ["sort", "--help"])
    assert result.exit_code == 0, f"output: {result.output}"

    # then (did nothing)
    for filepath in filepaths:
        assert os.path.exists(filepath)
        new_path = new_filepaths[filepath]
        assert not os.path.exists(new_path)

    # when
    result = cli_runner.invoke(cli_movie, ["sort"])
    assert result.exit_code == 0, f"output: {result.output}"

    # then (did rename matching directories)
    for filepath in filepaths:
        assert not os.path.exists(filepath)
        new_path = new_filepaths[filepath]
        assert os.path.exists(new_path)
