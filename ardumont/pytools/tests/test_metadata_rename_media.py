# Copyright (C) 2018-2021  The ardumont-pytools developers
# See the AUTHORS file at the top-level directory of this distribution
# License: GNU General Public License version 3, or any later version
# See top-level LICENSE file for more information

import os
from os.path import basename, exists, join
from shutil import copyfile

import pytest

from ardumont.pytools.dramatiq import CONF_ENV_VAR
from ardumont.pytools.media import to_media_info
from ardumont.pytools.utils import compute_destination_path

from .conftest import write_config_to_path


def test_metadata_rename_media(
    prepare_fs_with_family_media, stub_broker, stub_worker, ardumont_config, tmp_path
):
    """Rename family pictures/video to /volume/share/..."""
    from ardumont.pytools.tasks.rename_media import (  # noqa
        metadata_rename_media as task,
    )

    manifest, fake_slash = prepare_fs_with_family_media
    destination_path = join(fake_slash, "volume/share/pictures")

    with open(manifest, "r") as f:
        lst = f.readlines()
        for path in lst:
            src_media_path = join(fake_slash, path.rstrip())
            src_media_name = basename(src_media_path)

            # compute expected destination path
            dst_media_path = compute_destination_path(src_media_path, destination_path)

            assert exists(src_media_path) is True
            if exists(dst_media_path):
                os.unlink(dst_media_path)
            assert exists(dst_media_path) is False

            # just a hack to ease testing (VID_, IMG_ for proper files)
            if (
                src_media_name[0] not in ["V", "I"]
                or src_media_name.startswith(".")
                or src_media_name.endswith(".tmp")
            ):
                # noisy filenames (without properly formatted date)
                # will be ignored
                task.send(
                    src_media_path,
                    destination_path=destination_path,
                    dry_run=False,
                    cleanup=True,
                )
                assert exists(src_media_path) is True
                assert exists(dst_media_path) is False
                continue

            task(
                src_media_path,
                destination_path=destination_path,
                dry_run=True,
                cleanup=True,
            )
            # still exists because dry run
            assert exists(src_media_path) is True
            assert exists(dst_media_path) is False

            task(
                src_media_path,
                destination_path=destination_path,
                dry_run=False,
                cleanup=False,
            )
            # still exists because no cleanup
            assert exists(src_media_path) is True
            # new file exists because original got copied
            assert exists(dst_media_path) is True

            # remove just created path so we can check the cleanup behavior
            os.unlink(dst_media_path)
            assert exists(dst_media_path) is False

            # send as messages, will check a tad later
            task(
                src_media_path,
                destination_path=destination_path,
                dry_run=False,
                cleanup=True,
            )
            # no longer exists as it got renamed (well technically cleaned up)
            assert exists(src_media_path) is False
            # new file exists because original got copied
            assert exists(dst_media_path) is True


def test_tvshow_rename_media(
    prepare_fs_with_tvshows, stub_broker, stub_worker, sort_media_config, tmp_path
):
    from ardumont.pytools.tasks.rename_media import tvshow_rename_media as task

    manifest, fake_slash = prepare_fs_with_tvshows
    destination_path = join(fake_slash, "volume/share/tv-shows")

    assert exists(destination_path) is False

    with open(manifest, "r") as f:
        lst = f.readlines()
        for path in lst:
            src_media_path = join(fake_slash, path.rstrip())
            tvshow = to_media_info(path)
            title = tvshow.sanitized_title()
            season = tvshow.season_str()

            dst_media_path = join(
                destination_path, title[0], title, season, tvshow.name
            )
            assert exists(src_media_path) is True
            assert exists(dst_media_path) is False

            task(src_media_path, destination_path)

            assert exists(src_media_path) is False
            assert exists(dst_media_path) is True

    assert exists(destination_path) is True


def test_movie_rename_media(
    prepare_fs_with_movies, stub_broker, stub_worker, sort_media_config, tmp_path
):
    from ardumont.pytools.tasks.rename_media import movie_rename_media as task

    manifest, fake_slash = prepare_fs_with_movies
    destination_path = join(fake_slash, "volume/share/movies")

    assert exists(destination_path) is False

    with open(manifest, "r") as f:
        lst = f.readlines()
        for path in lst:
            src_media_path = join(fake_slash, path.rstrip())
            movie = to_media_info(src_media_path)

            dst_media_path = join(destination_path, movie.name)
            assert exists(src_media_path) is True
            assert exists(dst_media_path) is False

            task(src_media_path, destination_path)

            assert exists(src_media_path) is False
            assert exists(dst_media_path) is True

    assert exists(destination_path) is True


def test_rename_video_task(
    prepare_fs_with_tvshows, stub_broker, stub_worker, ardumont_config, tmp_path
):
    from ardumont.pytools.tasks.rename_media import (  # noqa
        rename_video_task as task,
    )

    manifest, fake_slash = prepare_fs_with_tvshows
    destination_path = join(fake_slash, "volume/share/tv-shows")

    converted_video_path = join(fake_slash, "volume/converted.mp4")
    assert exists(destination_path) is False
    assert exists(converted_video_path) is False

    task(destination_path, converted_video_path)
    assert exists(converted_video_path) is False

    os.makedirs(destination_path, exist_ok=True)
    task(destination_path, converted_video_path)

    with open(manifest, "r") as f:
        lst = f.readlines()
        for path in lst:
            src_media_path = join(fake_slash, path.rstrip())
            src_media_name = basename(src_media_path)

            dst_media_path = join(destination_path, src_media_name)
            assert exists(src_media_path) is True
            assert exists(dst_media_path) is False

            task(destination_path, src_media_path)

            assert exists(src_media_path) is False
            assert exists(dst_media_path) is True

    # reinit the src path
    copyfile(dst_media_path, src_media_path)
    # Create another file we will remove as well
    old_media_path = join(fake_slash, "old-path.mkv")
    copyfile(dst_media_path, old_media_path)

    os.unlink(dst_media_path)

    assert exists(src_media_path) is True
    assert exists(dst_media_path) is False
    assert exists(old_media_path) is True

    task(destination_path, src_media_path, old_video_path=old_media_path)

    assert exists(src_media_path) is False
    assert exists(dst_media_path) is True
    assert exists(old_media_path) is False
