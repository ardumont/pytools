# Copyright (C) 2018-2020  The ardumont-pytools developers
# See the AUTHORS file at the top-level directory of this distribution
# License: GNU General Public License version 3, or any later version
# See top-level LICENSE file for more information


def test_load_config_from_env(ardumont_config, ardumont_config_yaml):
    """Test loading configuration from environment

    """
    from ardumont.pytools.dramatiq import load_config_from_env

    actual_config = load_config_from_env()
    assert actual_config == ardumont_config_yaml


def test_dramatiq_init_broker(ardumont_config):
    """Test dramatiq application initialization

    """
    from ardumont.pytools.dramatiq import define_broker, load_config_from_env

    actual_config = load_config_from_env()

    broker = define_broker(actual_config)
    assert actual_config["dramatiq"]["broker"]["type"] == "stub"

    from dramatiq.brokers.stub import StubBroker

    assert isinstance(broker, StubBroker)

    from dramatiq import get_broker

    assert get_broker() == broker


def test_load_broker_from_env(ardumont_config):
    """Test pytools' dramatiq application initialization

    """
    from dramatiq.brokers.stub import StubBroker

    from ardumont.pytools.dramatiq import load_broker_from_env

    actual_broker = load_broker_from_env()
    assert isinstance(actual_broker, StubBroker)
    from dramatiq import get_broker

    assert get_broker() == actual_broker
