# Copyright (C) 2019-2020  The ardumont-pytools developers
# See the AUTHORS file at the top-level directory of this distribution
# License: GNU General Public License version 3, or any later version
# See top-level LICENSE file for more information

from datetime import datetime

from ardumont.pytools.date import (
    format_date,
    parse_date_from_filename,
    to_date,
)


def test_parse_date_from_filename():
    # parseable filenames renders a date
    for sep in [".", "_", "-", "?"]:
        for prefix in ["IMG", "PHOTO", "VID", "VIDEO", "", "whatever"]:
            for ext in [".jpg", ".mp4", "", ".mkv"]:
                for filename, expected_date in [
                    (
                        f"{prefix}{sep}20180511{sep}152438{ext}",
                        datetime(2018, 5, 11, 15, 24, 38),
                    ),
                    (
                        f"{prefix}{sep}20190629{sep}154239{sep}01{ext}",
                        datetime(2019, 6, 29, 15, 42, 39),
                    ),
                ]:
                    actual_date = parse_date_from_filename(filename)
                    assert actual_date == expected_date

    # unparsable filename
    for prefix in ["unknown_", "unknown"]:
        for ext in [".jpg", ".mp4", "", ".mkv"]:
            for filename in [
                f"{prefix}201805_152438{ext}",
                f"{prefix}201906_154239_01{ext}"
                f"{prefix}201906-154239_01{ext}"
                f"{prefix}201906--154239_01{ext}"
                f"{prefix}20190609--154239_01{ext}"
                f"{prefix}20190609_-154239_01{ext}"
                f"{prefix}1542_01{ext}"
                f"{prefix}nodate{ext}",
            ]:
                assert parse_date_from_filename(filename) is None


def test_to_date():
    for date, expected_date in [
        (None, None),
        ("00000511", None),
        (datetime(2018, 5, 11), datetime(2018, 5, 11)),
        ("20180511", datetime(2018, 5, 11)),
        ("2019:10:28 10:10:10", datetime(2019, 10, 28, 10, 10, 10)),
        ("2010:03:11", datetime(2010, 3, 11)),
    ]:
        assert to_date(date) == expected_date


def test_format_date():
    for date, expected_date in [
        (None, None),
        ("00000511", None),
        (datetime(2018, 5, 11), "20180511-000000"),
        ("20180511", "20180511-000000"),
        ("2019:10:28 10:10:10", "20191028-101010"),
        ("2010:03:11", "20100311-000000"),
    ]:
        assert format_date(date) == expected_date
