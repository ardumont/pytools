# Copyright (C) 2019-2020  The ardumont-pytools developers
# See the AUTHORS file at the top-level directory of this distribution
# License: GNU General Public License version 3, or any later version
# See top-level LICENSE file for more information

import logging

logger = logging.getLogger(__name__)


def test_hello(stub_broker, stub_worker):
    from dramatiq import Message

    from ardumont.pytools.tasks.hello import hello_task as task

    assert task.queue_name == "hello"
    assert task.broker == stub_broker

    msg = task.send("world")

    assert isinstance(msg, Message)
    assert msg.queue_name == task.queue_name
    assert msg.actor_name == "hello_task"

    logger.debug(f"msg: {msg} send on queue named '{task.queue_name}'")

    stub_broker.join(task.queue_name, fail_fast=True)
