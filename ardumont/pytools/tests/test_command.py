# Copyright (C) 2019  The ardumont-pytools developers
# See the AUTHORS file at the top-level directory of this distribution
# License: GNU General Public License version 3, or any later version
# See top-level LICENSE file for more information


from ardumont.pytools.command import exec_commands


def test_exec_commands_list_strings():
    cmds = ["echo hello", "echo test"]
    actual_result = exec_commands(cmds)
    assert actual_result


def test_exec_commands_mix_list_strings_list_list_strings():
    cmds = [["echo", "hello"], ["echo", "test"], "echo foobar"]
    actual_result = exec_commands(cmds)
    assert actual_result


def test_exec_commands_fail():
    cmds = [["echo", "hello"], ["ech"], "echo foobar"]  # not a good command
    actual_result = exec_commands(cmds)
    assert not actual_result


def test_exec_commands_failure():
    """Empty command to execute is considered a failure"""
    assert not exec_commands([])
