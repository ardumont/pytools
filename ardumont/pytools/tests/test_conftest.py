# Copyright (C) 2019-2021  The ardumont-pytools developers
# See the AUTHORS file at the top-level directory of this distribution
# License: GNU General Public License version 3, or any later version
# See top-level LICENSE file for more information

import os
from typing import List


def dirnames(path: str) -> List[str]:
    """List intermediary dirnames from a path

    """
    return list(filter(lambda x: x, os.path.dirname(path).split("/")))


def test_dirnames():
    """Check dirnames function behavior"""
    assert set(dirnames("/a/b/c/d.txt")) == set(["a", "b", "c"])
    assert set(dirnames("a/b/c/d.txt")) == set(["a", "b", "c"])
    assert set(dirnames("d.txt")) == set()


def test_fake_slash(datadir, prepare_fs_from_manifest):
    manifest, fake_slash = prepare_fs_from_manifest

    assert manifest == os.path.join(datadir, "manifest.txt")

    with open(manifest, "r") as f:
        files = f.readlines()
        all_filenames = []
        all_dirnames = []
        for line in files:
            filepath = line.rstrip()
            all_filenames.append(filepath)
            all_dirnames.extend(dirnames(filepath))

    current_dirnames = []
    current_filenames = []
    for _, _dirnames, _filenames in os.walk(fake_slash):
        current_dirnames.extend(_dirnames)
        current_filenames.extend(_filenames)

    assert set(current_filenames) == set([os.path.basename(f) for f in all_filenames])
    assert set(current_dirnames) == set(all_dirnames)
