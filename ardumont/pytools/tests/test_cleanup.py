# Copyright (C) 2018-2020  The ardumont-pytools developers
# See the AUTHORS file at the top-level directory of this distribution
# License: GNU General Public License version 3, or any later version
# See top-level LICENSE file for more information

import logging
from os.path import exists

logger = logging.getLogger(__name__)


def test_delete_video_task_delete(stub_broker, stub_worker, existing_filepath):
    from dramatiq import Message

    from ardumont.pytools.tasks.cleanup import delete_video_task as task

    assert task.queue_name == "cleanup"
    assert task.broker == stub_broker

    msg = task.send(existing_filepath)

    assert isinstance(msg, Message)
    assert msg.queue_name == task.queue_name
    assert msg.actor_name == "delete_video_task"

    logger.debug(f"msg: {msg} send on queue named '{task.queue_name}'")

    stub_broker.join(task.queue_name, fail_fast=True)

    assert not exists(existing_filepath)
