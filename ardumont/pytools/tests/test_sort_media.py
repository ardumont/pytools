# Copyright (C) 2018-2021  The ardumont-pytools developers
# See the AUTHORS file at the top-level directory of this distribution
# License: GNU General Public License version 3, or any later version
# See top-level LICENSE file for more information

from os.path import basename, join
from unittest.mock import call

from ardumont.pytools.media import to_media_info

from .conftest import existing_filepath_factory

# tvshow
existing_filepath0 = existing_filepath_factory("Awesome-tv-show-s03e10.avi")
# movie
existing_filepath1 = existing_filepath_factory("movie-2005.avi")
# tvshow
existing_filepath2 = existing_filepath_factory("even-more-so-s02e02.mkv")
# another tvshow
existing_filepath3 = existing_filepath_factory("Hello-s09e10.avi")


def test_sort_media(
    stub_broker,
    stub_worker,
    config_sort_media,
    sort_media_config,
    existing_filepath0,
    existing_filepath1,
    existing_filepath2,
    existing_filepath3,
    tmp_path,
    mocker,
):
    """All cases:
    - Movie detected                  -> rename as movie file
    - whitelisted tvshow detected     -> rename as tvshow file
    - not-whitelisted tvshow detected -> rename as file

    """
    from dramatiq import Message

    from ardumont.pytools.tasks.sort_media import sort_media_task as task

    destination_path = config_sort_media["movie"]["destination"]

    mock_tvshow = mocker.patch(
        "ardumont.pytools.tasks.rename_media.tvshow_rename_media"
    )
    mock_movie = mocker.patch("ardumont.pytools.tasks.rename_media.movie_rename_media")
    mock_known_tvshow = mocker.patch(
        "ardumont.pytools.tasks.sort_media.is_known_tvshow"
    )

    all_paths = [
        {"path": existing_filepath0, "type": "tvshow"},
        {"path": existing_filepath1, "type": "movie"},
        {"path": existing_filepath2, "type": "tvshow"},
        {"path": existing_filepath3, "type": "tvshow"},
    ]

    expected_call_history = {
        "movie": [],
        "tvshow": [],
    }

    for media in all_paths:
        video_path = media["path"]

        media_type = media["type"]
        media_info = to_media_info(video_path)
        title = str(media_info)
        if media_type == "tvshow":  # Pretend those tvshows are ok
            mock_known_tvshow.return_value = True
            destination_path = config_sort_media["tvshow"]["destination"]
            one_call = call(video_path, destination_path=destination_path)
        elif media["type"] == "movie":  # movie ok
            destination_path = config_sort_media["movie"]["destination"]
            one_call = call(video_path, destination_path=destination_path)

        expected_call_history[media_type].append(one_call)

        msg = task.send(video_path, config=config_sort_media)

        assert isinstance(msg, Message)
        assert msg.queue_name == "sort"
        assert task.queue_name == "sort"
        assert msg.actor_name == "sort_media_task"

    stub_broker.join(task.queue_name, fail_fast=True)

    assert_set_ok(mock_movie.call_args_list, expected_call_history["movie"])
    assert_set_ok(mock_tvshow.call_args_list, expected_call_history["tvshow"])

    assert mock_movie.called and mock_tvshow.called


def assert_set_ok(actual_records, expected_records):
    assert len(actual_records) == len(expected_records)
    for actual_record in actual_records:
        assert actual_record in expected_records
