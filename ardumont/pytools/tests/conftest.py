# Copyright (C) 2019-2021  The ardumont-pytools developers
# See the AUTHORS file at the top-level directory of this distribution
# License: GNU General Public License version 3, or any later version
# See top-level LICENSE file for more information

import logging
import os
from os.path import exists, join
from typing import Dict

from click.testing import CliRunner
import pytest
from tvmaze.api import Api
from tvmaze.models import Show
import yaml

from ardumont.pytools.dramatiq import CONF_ENV_VAR

logger = logging.getLogger(__name__)


@pytest.fixture
def cli_runner():
    return CliRunner()


@pytest.fixture
def ardumont_config_yaml():
    logger.debug("Define default configuration")
    return {
        "dramatiq": {
            "broker": {
                "type": "stub",  # prod: redis
                "queues": ["rename-media", "cleanup", "sort", "hello", "rename-tvshow"],
            },
        },
    }


def write_config_to_path(tmp_path: str, name: str, config_dict: Dict) -> str:
    conf_path = os.path.join(tmp_path, name)
    with open(conf_path, "w") as f:
        f.write(yaml.dump(config_dict))
    return conf_path


@pytest.fixture
def ardumont_config(ardumont_config_yaml, monkeypatch, tmp_path):
    conf_path = write_config_to_path(tmp_path, "default.yaml", ardumont_config_yaml)
    monkeypatch.setenv(f"{CONF_ENV_VAR}", conf_path)
    return conf_path


@pytest.fixture
def stub_broker(ardumont_config):
    logger.debug("Load broker configuration from environment")
    from ardumont.pytools.dramatiq import load_broker_from_env

    broker = load_broker_from_env()
    broker.emit_after("process_boot")
    broker.flush_all()
    return broker


@pytest.fixture
def stub_worker(stub_broker):
    logger.debug("Define and start worker working with defined broker")
    from dramatiq import Worker

    worker = Worker(
        stub_broker, queues=stub_broker.queues, worker_timeout=10, worker_threads=2
    )
    worker.start()
    yield worker
    worker.stop()


def existing_filepath_factory(filename):
    @pytest.fixture
    def existing_filepath(tmp_path, filename=filename):
        something_path = str(tmp_path / filename)
        with open(something_path, "w") as f:
            f.write(f"it's a thing named {filename}!")
        assert exists(something_path)
        return something_path

    return existing_filepath


existing_filepath = existing_filepath_factory("something")


@pytest.fixture
def config_sort_media(tmp_path):
    destination_movies = str(tmp_path / "movies")
    os.makedirs(destination_movies, exist_ok=True)
    destination_tvshows = str(tmp_path / "tv-shows")
    os.makedirs(destination_tvshows, exist_ok=True)
    config = {
        "application-command": {
            "config-path": "/etc/app/app.json",
            "command": "/path/to/test-tvtool",
            "tvshow-id-filename": ".tvmaze-id",
        },
        "movie": {
            "destination": destination_movies,
        },
        "tvshow": {
            "destination": destination_tvshows,
        },
    }
    return config


@pytest.fixture
def sort_media_config(ardumont_config_yaml, config_sort_media, monkeypatch, tmp_path):
    config_dict = {**ardumont_config_yaml, **config_sort_media}
    conf_path = write_config_to_path(tmp_path, "sort-media.yaml", config_dict)
    monkeypatch.setenv(f"{CONF_ENV_VAR}", conf_path)
    return conf_path


KNOWN_TVSHOWS = ["ToNyX", "rocks!", "Awesome TV-Show"]


def make_show(name, id, **kwargs):
    return Show.parse(dict(name=name, id=id, **kwargs))


def mock_search_fn(tvmaze_instance: Api, name: str) -> Show:
    """Simulate a search which returns something in KNOWN_TVSHOWS"""
    if name in KNOWN_TVSHOWS:
        return make_show(name, KNOWN_TVSHOWS.index(name))


@pytest.fixture
def mock_search_tvshow(mocker):
    mock_search = mocker.patch("ardumont.pytools.media.search_tvshow")
    mock_search.side_effect = mock_search_fn


@pytest.fixture
def datadir(request):
    """Return data test directory."""
    return join(request.fspath.dirname, "data")


def _prepare_fs(manifest: str, root_dir: str) -> None:
    """Prepare filesystem for tests"""
    with open(manifest, "r") as f:
        lst = f.readlines()
        for path in lst:
            fullpath = os.path.join(root_dir, path.rstrip())
            dirpath = os.path.dirname(fullpath)
            # create folder
            os.makedirs(dirpath, exist_ok=True)
            # create empty file
            with open(fullpath, "w") as temp_file:
                temp_file.write("")


@pytest.fixture
def prepare_fs_from_manifest(datadir, tmp_path):
    manifest = os.path.join(datadir, "manifest.txt")
    fake_slash = os.path.join(tmp_path, "fake-slash")
    _prepare_fs(manifest, fake_slash)
    return manifest, fake_slash


@pytest.fixture
def prepare_fs_with_movies(datadir, tmp_path):
    manifest = os.path.join(datadir, "manifest-movies.txt")
    fake_slash = os.path.join(tmp_path, "fake-slash")
    _prepare_fs(manifest, fake_slash)
    return manifest, fake_slash


@pytest.fixture
def prepare_fs_with_family_media(datadir, tmp_path):
    manifest = os.path.join(datadir, "manifest-family-media.txt")
    fake_slash = os.path.join(tmp_path, "fake-slash")
    _prepare_fs(manifest, fake_slash)
    return manifest, fake_slash


@pytest.fixture
def prepare_fs_with_tvshows(datadir, tmp_path):
    manifest = os.path.join(datadir, "manifest-tvshows.txt")
    fake_slash = os.path.join(tmp_path, "fake-slash")
    _prepare_fs(manifest, fake_slash)
    return manifest, fake_slash
