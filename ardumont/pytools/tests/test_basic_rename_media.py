# Copyright (C) 2018-2021  The ardumont-pytools developers
# See the AUTHORS file at the top-level directory of this distribution
# License: GNU General Public License version 3, or any later version
# See top-level LICENSE file for more information

import logging
from os.path import basename, exists, join

from .conftest import existing_filepath_factory

logger = logging.getLogger(__name__)

# movie
existing_filepath1 = existing_filepath_factory("movie-2005.avi")


def test_basic_rename_media(
    stub_broker, stub_worker, existing_filepath, existing_filepath1, tmp_path
):
    from dramatiq import Message

    from ardumont.pytools.tasks.basic_rename_media import (
        basic_rename_media as task,
    )

    assert task.queue_name == "rename-media"
    assert task.broker == stub_broker, "task broker must be the tests' one"

    destination_path = tmp_path / "dir"
    destination_path.mkdir(exist_ok=True)

    for path, new_name in [(existing_filepath, None), (existing_filepath1, "foo")]:
        filename = basename(path)
        new_filepath = join(
            destination_path, filename if new_name is None else new_name
        )
        assert not exists(new_filepath)

        # rename existing filepath to the destination
        msg = task.send(
            path,
            destination_path=str(destination_path),
            dest_name=new_name,
            sanitize=False,
        )

        assert isinstance(msg, Message)
        assert msg.queue_name == task.queue_name
        assert msg.actor_name == "basic_rename_media"

        stub_broker.join(task.queue_name, fail_fast=True)
        stub_worker.join()

        # when done, the file should have been moved
        assert not exists(existing_filepath)
        assert exists(new_filepath)
