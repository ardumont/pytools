# Copyright (C) 2018-2020  The ardumont-pytools developers
# See the AUTHORS file at the top-level directory of this distribution
# License: GNU General Public License version 3, or any later version
# See top-level LICENSE file for more information

import logging
from os.path import exists, join

import pytest

from ardumont.pytools.producer import (
    PytoolsEventHandler,
    exclude_path,
    make_notifier,
)
from ardumont.pytools.producer.convert_video import (
    EXTENSIONS_TO_CHECK,
    ConvertEventHandler,
    ConvertVideoEventHandler,
)
from ardumont.pytools.producer.rename_media import RenamingEventHandler
from ardumont.pytools.producer.rotate_video import RotateVideoEventHandler
from ardumont.pytools.utils import is_video

logger = logging.getLogger(__name__)


class FakeEvent:
    def __init__(self, pathname):
        self.pathname = pathname


@pytest.mark.usefixtures("stub_broker")  # to avoid dramatiq's state mess
def test_rename_media_handler(tmp_path, mocker):
    """When event is triggered, sent for renaming message is sent

    """
    mock_module = mocker.patch(
        "ardumont.pytools.tasks.rename_media.metadata_rename_media"
    )
    mock_module.send.return_value = {}
    d_path = str(tmp_path)
    handler = RenamingEventHandler(destination_path=d_path, cleanup=False)
    assert isinstance(handler, PytoolsEventHandler)

    pathname = "event.pathname"
    actual_result = handler._process_event(FakeEvent(pathname))

    assert actual_result is None

    assert mock_module.send.has_calls(pathname, destination_path=d_path, cleanup=False)


@pytest.mark.usefixtures("stub_broker")  # to avoid dramatiq's state mess
def test_rotate_video_handler(tmp_path, mocker):
    """When recognized as video, rotation message is sent

    """
    mock_module = mocker.patch("ardumont.pytools.tasks.rotate_video.rotate_video_task")
    mock_module.send.return_value = {}
    d_path = str(tmp_path)
    handler = RotateVideoEventHandler(destination_path=d_path, cleanup=False)
    assert isinstance(handler, PytoolsEventHandler)

    pathname = "/path/to/180/some-video.mp4"
    actual_result = handler._process_event(FakeEvent(pathname))

    assert actual_result is None

    assert mock_module.send.has_calls(pathname, rotation=180, destination_path=d_path)


@pytest.mark.usefixtures("stub_broker")  # to avoid dramatiq's state mess
def test_rotate_video_handler_not_a_video(tmp_path):
    """When not recognized as a video, file is cleaned up immediately

    """
    pathname = join(str(tmp_path), "something")
    with open(pathname, "w") as f:
        f.write("")
    assert exists(pathname)
    assert not is_video(pathname)

    d_path = str(tmp_path)
    handler = RotateVideoEventHandler(destination_path=d_path)
    assert isinstance(handler, PytoolsEventHandler)

    actual_result = handler._process_event(FakeEvent(pathname))

    # pathname is not recognized as a video file, it's cleaned up directly
    assert actual_result is None
    assert not exists(pathname)


@pytest.mark.usefixtures("stub_broker")  # to avoid dramatiq's state mess
def test_convert_video_handler_video(tmp_path, mocker):
    """When recognized as a video, convert message should be sent

    """
    mock_module = mocker.patch(
        "ardumont.pytools.tasks.convert_video.convert_video_task.send"
    )
    mock_module.return_value = {}
    for video_ext in EXTENSIONS_TO_CHECK:
        pathname = join(str(tmp_path), f"something.{video_ext}")
        with open(pathname, "w") as f:
            f.write("")
        assert exists(pathname)
        assert is_video(pathname, extensions_to_check=EXTENSIONS_TO_CHECK)

        keep_metadata = True
        handler = ConvertVideoEventHandler(keep_metadata=keep_metadata)
        assert isinstance(handler, PytoolsEventHandler)

        actual_result = handler._process_event(FakeEvent(pathname))

        # pathname is not recognized as a video file, it's cleaned up directly
        assert actual_result is None
        assert mock_module.has_calls(pathname, keep_metadata=keep_metadata)
        mock_module.reset_mock()


@pytest.mark.usefixtures("stub_broker")  # to avoid dramatiq's state mess
def test_convert_video_handler_not_video(tmp_path, mocker):
    """When not recognized as a video, nothing happens

    """
    mock_module = mocker.patch(
        "ardumont.pytools.tasks.convert_video.convert_video_task.send"
    )
    mock_module.return_value = {}
    for video_ext in ["txt", "lst"]:
        pathname = join(str(tmp_path), f"something.{video_ext}")
        with open(pathname, "w") as f:
            f.write("")
        assert exists(pathname)
        assert not is_video(pathname, extensions_to_check=EXTENSIONS_TO_CHECK)

        keep_metadata = True
        handler = ConvertVideoEventHandler(keep_metadata=keep_metadata)
        assert isinstance(handler, PytoolsEventHandler)

        actual_result = handler._process_event(FakeEvent(pathname))

        # pathname is not recognized as a video file, it's cleaned up directly
        assert actual_result is None
        assert not mock_module.called


@pytest.mark.usefixtures("stub_broker")  # to avoid dramatiq's state mess
def test_convert_event_handler_video(tmp_path, mocker):
    """When recognized as a video, convert video message is sent

    """
    mock_module = mocker.patch(
        "ardumont.pytools.tasks.convert_video.convert_video_task.send"
    )
    mock_module.return_value = {}
    for video_ext in EXTENSIONS_TO_CHECK:
        pathname = join(str(tmp_path), f"something.{video_ext}")
        with open(pathname, "w") as f:
            f.write("")
        assert exists(pathname)
        assert is_video(pathname, extensions_to_check=EXTENSIONS_TO_CHECK)

        d_path = str(tmp_path)
        handler = ConvertEventHandler(destination_path=d_path)
        assert isinstance(handler, PytoolsEventHandler)

        actual_result = handler._process_event(FakeEvent(pathname))

        # pathname is not recognized as a video file, it's cleaned up directly
        assert actual_result is None
        assert mock_module.has_calls(pathname)
        mock_module.reset_mock()


@pytest.mark.usefixtures("stub_broker")  # to avoid dramatiq's state mess
def test_convert_event_handler_media(tmp_path, mocker):
    """When not recognized as a video, rename media message is sent

    """
    mock_module = mocker.patch(
        "ardumont.pytools.tasks.basic_rename_media.basic_rename_media.send"
    )
    mock_module.return_value = {}
    for file_ext in ["txt", "lst", "ogg"]:
        pathname = join(str(tmp_path), f"something.{file_ext}")
        with open(pathname, "w") as f:
            f.write("")
        assert exists(pathname)
        assert not is_video(pathname, extensions_to_check=EXTENSIONS_TO_CHECK)

        d_path = str(tmp_path)
        handler = ConvertEventHandler(destination_path=d_path)
        assert isinstance(handler, PytoolsEventHandler)

        actual_result = handler._process_event(FakeEvent(pathname))

        # pathname is not recognized as a video file, it's cleaned up directly
        assert actual_result is None
        assert mock_module.has_calls(pathname, destination_path=d_path)
        mock_module.reset_mock()


def test_exclude_path():
    for path in [".", ".tmp"]:  # excluded path should be True
        assert exclude_path(path)

    for path in ["other-paths", "anything", "/really"]:
        assert not exclude_path(path)


def test_make_notifier(mocker, tmp_path):
    mock_watch_manager = mocker.patch("ardumont.pytools.producer.WatchManager")
    mock_watch_manager.add_watch.return_value = {}

    mock_notifier = mocker.patch("ardumont.pytools.producer.Notifier")
    handler = RenamingEventHandler(destination_path=str(tmp_path))

    paths = []
    for dirpath in ["some", "things", "never", "changed"]:
        path = tmp_path / dirpath
        path.mkdir()
        paths.append(str(path))

    actual_notifier = make_notifier(handler, paths)

    assert actual_notifier

    assert mock_watch_manager.has_calls()
    assert mock_notifier.has_calls()
