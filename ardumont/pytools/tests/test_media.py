# Copyright (C) 2019-2021  The ardumont-pytools developers
# See the AUTHORS file at the top-level directory of this distribution
# License: GNU General Public License version 3, or any later version
# See top-level LICENSE file for more information

from collections import OrderedDict
import logging
from os.path import join

import pytest
import yaml

from ardumont.pytools.media import (
    is_known_tvshow,
    prepare_tvshow,
    to_media_info,
)
from ardumont.pytools.model import MediaInfo, MovieInfo, TVShowInfo

from .conftest import KNOWN_TVSHOWS, make_show

logger = logging.getLogger(__name__)


@pytest.mark.usefixtures("mock_search_tvshow")
def test_is_known_tvshow_not_show():
    for unknown_tvshow in ["some", "tvshow", "not", "referenced", None, ""]:
        assert not is_known_tvshow(unknown_tvshow)


@pytest.mark.usefixtures("mock_search_tvshow")
def test_is_known_tvshow_show():
    for unknown_tvshow in KNOWN_TVSHOWS:
        assert is_known_tvshow(unknown_tvshow)


def test_prepare_tvshow(datadir):
    meta_file = join(datadir, "tvshow-meta.yaml")
    tvshow_meta = yaml.safe_load(open(meta_file).read())

    assert isinstance(tvshow_meta, list)
    expected_tvshows = [
        OrderedDict(
            [
                ("name", "Lost in Space"),
                ("tvmaze_id", 72923),
                ("premiered", "1965-9-15"),
                ("language", "en"),
                ("status", "Ended"),
            ]
        ),
        OrderedDict(
            [
                ("name", "in Space Lost"),
                ("tvmaze_id", 343253),
                ("premiered", "2018-4-13"),
                ("language", "en"),
                ("status", "Continuing"),
            ]
        ),
        OrderedDict(
            [
                ("name", "The Robinsons: Lost in Space"),
                ("tvmaze_id", 248282),
                ("premiered", "2008-11-20"),
                ("language", "en"),
                ("status", "Ended"),
            ]
        ),
        OrderedDict(
            [
                ("name", "Lose in Space"),
                ("tvmaze_id", 348509),
                ("language", "en"),
                ("status", "Ended"),
            ]
        ),
    ]

    assert len(tvshow_meta) == len(expected_tvshows)
    for i, tvshow in enumerate(tvshow_meta):
        show = make_show(**tvshow)
        assert show is not None
        actual_tvshow = prepare_tvshow(show)
        assert actual_tvshow == expected_tvshows[i]


@pytest.mark.parametrize(
    "media_file,expected_media_info,expected_str",
    [
        (
            "/some/path/Awesome Journey.1920.avi",
            MovieInfo(
                path="/some/path/Awesome Journey.1920.avi",
                name="Awesome Journey.1920.avi",
                title="Awesome Journey",
                year=1920,
                extension="avi",
            ),
            "awesome-journey-1920.avi",
        ),
        (
            "/some/path/only-the-fools.1990.mp4",
            MovieInfo(
                path="/some/path/only-the-fools.1990.mp4",
                name="only-the-fools.1990.mp4",
                title="only-the-fools",
                year=1990,
                extension="mp4",
            ),
            "only-the-fools-1990.mp4",
        ),
        (
            "extraordinary-s03e09.mkv",
            TVShowInfo(
                path="extraordinary-s03e09.mkv",
                name="extraordinary-s03e09.mkv",
                title="extraordinary",
                extension="mkv",
                season=3,
                episode=9,
            ),
            "extraordinary-s03e09.mkv",
        ),
    ],
)
def test_media_info(media_file, expected_media_info, expected_str):
    actual_media_info = to_media_info(media_file)
    assert actual_media_info == expected_media_info
    assert isinstance(actual_media_info, MediaInfo)
    assert str(actual_media_info) == expected_str
