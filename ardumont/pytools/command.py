# Copyright (C) 2016-2020  The ardumont-pytools developers
# See the AUTHORS file at the top-level directory of this distribution
# License: GNU General Public License version 3, or any later version
# See top-level LICENSE file for more information

import logging
import subprocess
from typing import List, Union

logger = logging.getLogger(__name__)


def exec_commands(cmds: Union[List[str], List[List[str]]]) -> bool:
    """Execute the list of commands. If one fails, stop."""
    if not cmds:
        logger.warning("No command to execute, failing then.")
        return False
    for scmd in cmds:
        if isinstance(scmd, str):
            cmd = scmd.split(" ")
        else:
            cmd = scmd
        try:
            subprocess.run(
                cmd, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL, check=True
            )
        except Exception as e:
            logger.error("Fail to execute %s. Reason: %s", cmd, e)
            return False
    return True
