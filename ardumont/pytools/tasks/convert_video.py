# Copyright (C) 2018-2020  The ardumont-pytools developers
# See the AUTHORS file at the top-level directory of this distribution
# License: GNU General Public License version 3, or any later version
# See top-level LICENSE file for more information

"""Orchestrate video conversion

"""

import logging
import os
import subprocess
from typing import Any, Dict, Mapping

import dramatiq
from dramatiq.middleware import TimeLimitExceeded

from ardumont.pytools import metadata

from .cleanup import delete_video_task
from .rename_media import rename_video_task

logger = logging.getLogger(__name__)


FILTERED_EXTENSIONS = [
    "mkv",
    "mov",
    "webm",
    "3gp",
    "3gpp",
    "avi",
    "m4v",
    "mpeg",
    "mpg",
    "ogg",
    "dv",
    "mp4",
]


def compute_converted_video_path(
    video_path: str, to_video_format: str, work_folder: str = "convert"
) -> str:
    video_base_path, extension = os.path.splitext(video_path)
    dir_path = os.path.dirname(video_path)
    work_path = os.path.join(dir_path, work_folder)
    video_filename = os.path.basename(video_base_path)
    converted_video_path = f"{work_path}/{video_filename}.{to_video_format}"
    return converted_video_path


def convert_video(
    video_path: str,
    work_folder: str = "convert",
    to_video_format: str = "mp4",
    to_audio_format: str = "mp3",
    keep_metadata: bool = True,
    config: Mapping[str, Any] = {},
):
    """Convert a pre-formatted video_path (cf. FILTERED_EXTENSIONS) to
       format in work_folder.

       If a previous dangling work_folder file with same name exists,
       it is removed prior to the new conversion work.

    Returns:
       None if the video_path is not a mkv video (as per extension file).
       False if a problem occurred during conversion.

       a dict with keys 'dir_path', 'video' holding the information
       for the next processing steps.

    """
    if not config:
        from ardumont.pytools.dramatiq import load_config_from_env

        config = load_config_from_env()

    avconv_command = config.get("avconv", {}).get("command", "avconv")

    if not os.path.exists(video_path):
        logger.warning("Input file %s does not exist. Skipping." % video_path)
        return {
            "next_action": None,
        }

    video_base_path, extension = os.path.splitext(video_path)
    extension = extension.split(".")[1]
    if extension.lower() not in FILTERED_EXTENSIONS:
        logger.warning(
            "Extension supposed to be one of %s. Skipping"
            % (", ".join(FILTERED_EXTENSIONS),)
        )
        return {
            "next_action": None,
        }

    dir_path = os.path.dirname(video_path)
    work_path = os.path.join(dir_path, work_folder)
    os.makedirs(work_path, exist_ok=True)

    existing_video_path = "{video_base_path}.{to_video_format}"
    if os.path.exists(existing_video_path):
        logger.warning(
            "Existing converted file %s. Cleaning up old file %s.",
            existing_video_path,
            video_path,
        )
        return {
            "next_action": "delete",
        }

    converted_video_path = compute_converted_video_path(
        video_path, to_video_format, work_folder
    )

    if os.path.exists(converted_video_path):
        os.unlink(converted_video_path)

    cmd = [avconv_command, "-i", video_path, "-c:a", "mp3"]
    if keep_metadata:
        cmd.extend(["-map_metadata", "0:s:0"])
    cmd.append(converted_video_path)

    logger.info("Convert: %s" % " ".join(cmd))
    try:
        # don't want any output success/error
        r = subprocess.run(cmd, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
        if r.returncode != 0:
            logger.warning(
                "Conversion failure from %s to %s. Skipping."
                % (video_path, converted_video_path)
            )
            return {"next_action": None}
    except subprocess.CalledProcessError:
        logger.warning(
            "Conversion error from %s to %s. Skipping."
            % (video_path, converted_video_path)
        )
        return {"next_action": None}

    if not keep_metadata:
        metadata.clean(converted_video_path)

    return {
        "next_action": "rename",
        "args": {
            "destination_path": dir_path,
            "converted_video_path": converted_video_path,
            "old_video_path": video_path,
        },
    }


@dramatiq.actor(queue_name="convert", time_limit=60 * 60 * 1000)
def convert_video_task(
    video_path: str,
    to_video_format: str = "mp4",
    work_folder: str = "convert",
    keep_metadata: bool = True,
):
    """Convert a video path (timeout of 1h)

    """
    converted_video_path = compute_converted_video_path(
        video_path, to_video_format, work_folder
    )
    data: Dict = {}
    try:
        data = convert_video(video_path, keep_metadata=keep_metadata)
    except TimeLimitExceeded:
        # Clean up temporary state
        if os.path.exists(converted_video_path):
            os.unlink(converted_video_path)
    else:
        next_action = data.get("next_action")
        if not next_action:
            return

        if next_action == "rename":
            # orchestrate the removal of the old video
            # and rename the converted video to the new one
            rename_video_task.send(**data["args"])
        elif next_action == "delete":
            delete_video_task.send(video_path)
        else:
            raise ValueError("Programmatic error: unexpected action!")
