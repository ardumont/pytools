# Copyright (C) 2018-2021  The ardumont-pytools developers
# See the AUTHORS file at the top-level directory of this distribution
# License: GNU General Public License version 3, or any later version
# See top-level LICENSE file for more information

"""Orchestrate media renaming"""

import logging
import os
from typing import Callable, Optional

import dramatiq

from ardumont.pytools import metadata, utils

logger = logging.getLogger(__name__)


@dramatiq.actor(queue_name="rename-media")
def basic_rename_media(
    src_path: str,
    destination_path: str = "/volume/share/camera",
    sanitize: bool = False,
    dest_name: Optional[str] = None,
    post_process: Callable = lambda path: None,
):
    """Basic media renaming orchestration.
    Check for clash.

    """
    if not os.path.exists(src_path):
        logger.warning("%s skipped: inexistent.", src_path)
        return

    if sanitize:  # clean up noisy metadata/characters
        sane_path = utils.sanitize(metadata.clean(src_path))
    else:
        sane_path = src_path

    if dest_name is None:
        dest_name = os.path.basename(sane_path)

    dst_path = os.path.join(destination_path, dest_name)
    utils.copy_or_rename_with_clash(src_path, dst_path, cleanup=True)
    post_process(destination_path)
