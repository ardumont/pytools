# Copyright (C) 2018-2020  The ardumont-pytools developers
# See the AUTHORS file at the top-level directory of this distribution
# License: GNU General Public License version 3, or any later version
# See top-level LICENSE file for more information

"""Orchestrate video conversion"""

import logging
import os
import subprocess

import dramatiq

from .rename_media import rename_video_task

logger = logging.getLogger(__name__)


POSSIBLE_ROTATIONS_CW = [90, 180, 270]


def rotate_video(video_path, nb_rotations, work_folder="rotate", destination_path=None):
    """Rotate a video_path nb_rotations clockwise in work_folder.

       If a previous dangling work_folder file with same name exists,
       it is removed prior to the new rotation work.

    Returns:
       a dict with the following actions to do.

       a dict with keys 'dir_path', 'video' holding the information
       for the next processing steps.

    """
    if not os.path.exists(video_path):
        logger.warning("Input file %s does not exist. Skipping." % video_path)
        return {
            "next_action": None,
        }

    video_filename = os.path.basename(video_path)
    dir_path = os.path.dirname(video_path)
    work_path = os.path.join(dir_path, work_folder)
    os.makedirs(work_path, exist_ok=True)

    converted_video_path = os.path.join(work_path, video_filename)
    if os.path.exists(converted_video_path):
        os.unlink(converted_video_path)

    rotate_filter = ",".join(["transpose=1," * nb_rotations]).rstrip(",")
    cmd = [
        "avconv",
        "-i",
        video_path,
        "-vf",
        rotate_filter,
        "-map_metadata",
        "0:s:0",
        converted_video_path,
    ]

    logger.info("Rotate: %s" % " ".join(cmd))

    try:
        # don't want any output success/error
        r = subprocess.run(cmd, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
        if r.returncode != 0:
            logger.warning(
                "Rotation failure from %s to %s. Skipping."
                % (video_path, converted_video_path)
            )
            return {"next_action": None}
    except subprocess.CalledProcessError:
        logger.warning(
            "Rotation error from %s to %s. Skipping."
            % (video_path, converted_video_path)
        )
        return {"next_action": None}

    destination_path = destination_path if destination_path else dir_path
    return {
        "next_action": "rename",
        "args": {
            "destination_path": destination_path,
            "converted_video_path": converted_video_path,
        },
    }


@dramatiq.actor(queue_name="rotate")
def rotate_video_task(video_path, rotation=90, destination_path=None):
    """Rotate video clockwise.

    """
    if isinstance(rotation, str):
        rotation = int(rotation)

    if rotation not in POSSIBLE_ROTATIONS_CW:
        logger.error("rotation must be in %s" % ("".join(POSSIBLE_ROTATIONS_CW),))
        return

    nb_rotations = int(rotation / 90)

    data = rotate_video(
        video_path, nb_rotations=nb_rotations, destination_path=destination_path
    )
    if not data or not data.get("next_action"):
        return

    next_action = data.get("next_action")
    if next_action == "rename":
        rename_video_task.send(**data["args"])
    else:
        raise ValueError("Programmatic error: unexpected action!")
