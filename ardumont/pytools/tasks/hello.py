# Copyright (C) 2018-2020  The ardumont-pytools developers
# See the AUTHORS file at the top-level directory of this distribution
# License: GNU General Public License version 3, or any later version
# See top-level LICENSE file for more information

"""Just hello program

"""

import logging

import dramatiq

logger = logging.getLogger(__name__)


@dramatiq.actor(queue_name="hello")
def hello_task(name):
    logger.info(f"hello {name}")
    return name
