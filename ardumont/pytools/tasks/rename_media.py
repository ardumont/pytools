# Copyright (C) 2018-2021  The ardumont-pytools developers
# See the AUTHORS file at the top-level directory of this distribution
# License: GNU General Public License version 3, or any later version
# See top-level LICENSE file for more information

"""Orchestrate media renaming"""

import logging
import os

import dramatiq

from ardumont.pytools import utils
from ardumont.pytools.media import is_known_tvshow, to_media_info

logger = logging.getLogger(__name__)


@dramatiq.actor(queue_name="rename-tvshow")
def tvshow_rename_media(src_path, destination_path="/volume/share/camera"):
    """TVShow Media renaming task."""
    tvshow = to_media_info(src_path)
    title = tvshow.sanitized_title()
    season = tvshow.season_str()
    dest_path = os.path.join(destination_path, title[0], title, season)

    from .basic_rename_media import basic_rename_media

    return basic_rename_media(src_path, dest_path, post_process=utils.tvshow_rename)


@dramatiq.actor(queue_name="rename-movie")
def movie_rename_media(src_path, destination_path="/volume/share/camera"):
    """Movie Media renaming task."""
    movie = to_media_info(src_path)
    title = str(movie)
    from .basic_rename_media import basic_rename_media

    return basic_rename_media(src_path, destination_path, dest_name=title)


@dramatiq.actor(queue_name="rename-metadata")
def metadata_rename_media(
    src_path, destination_path="/volume/share/pictures", cleanup=False, dry_run=False
):
    """Rename media according to metadata held in the media file."""
    if dry_run:
        logger.warning("** dry run **")

    src_name = os.path.basename(src_path)
    if src_name.startswith(".") or src_name.endswith(".tmp"):
        logger.warning("%s skipped: temporary file", src_path)
        return

    if not os.path.exists(src_path):
        logger.warning("%s skipped: does not exist", src_path)
        return

    dest_path = utils.compute_destination_path(src_path, destination_path)
    if not dest_path:
        logger.warning("%s skipped: no date detected", src_path)
        return

    if dry_run:
        logger.info("%s %s %s", "mv" if cleanup else "cp", src_path, dest_path)
        return

    utils.copy_or_rename_with_clash(src_path, dest_path, cleanup=cleanup)


@dramatiq.actor(queue_name="rename-video")
def rename_video_task(destination_path, converted_video_path, old_video_path=None):
    """Rename video and clean up"""
    if not os.path.exists(destination_path):
        logger.warning(
            "%s does not exist, could not rename %s to %s",
            destination_path,
            converted_video_path,
            destination_path,
        )
        return

    if not os.path.exists(converted_video_path):
        logger.warning(
            "%s does not exist, could not rename %s to %s!",
            converted_video_path,
            converted_video_path,
            destination_path,
        )
        return

    new_video_filename = os.path.basename(converted_video_path)
    new_video_path_final = os.path.join(destination_path, new_video_filename)

    logger.info("Rename: mv %s %s", converted_video_path, new_video_path_final)
    os.rename(converted_video_path, new_video_path_final)
    if old_video_path:
        from .cleanup import delete_video_task

        delete_video_task(old_video_path)
