# Copyright (C) 2018-2020  The ardumont-pytools developers
# See the AUTHORS file at the top-level directory of this distribution
# License: GNU General Public License version 3, or any later version
# See top-level LICENSE file for more information

import logging
import os

import dramatiq

logger = logging.getLogger(__name__)


@dramatiq.actor(queue_name="cleanup")
def delete_video_task(old_video_path):
    if os.path.exists(old_video_path):
        logger.info("Cleanup %s" % old_video_path)
        os.unlink(old_video_path)
