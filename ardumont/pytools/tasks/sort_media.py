# Copyright (C) 2018-2021  The ardumont-pytools developers
# See the AUTHORS file at the top-level directory of this distribution
# License: GNU General Public License version 3, or any later version
# See top-level LICENSE file for more information

"""Orchestrate media sorting/renaming

"""

import logging
import os
from typing import Any, Mapping

import dramatiq

from ardumont.pytools.media import is_known_tvshow, to_media_info
from ardumont.pytools.model import MovieInfo, TVShowInfo
from ardumont.pytools.utils import sanitize_string

logger = logging.getLogger(__name__)


@dramatiq.actor(queue_name="sort")
def sort_media_task(video_path: str, config: Mapping[str, Any] = {}):
    if not config:
        from ardumont.pytools.dramatiq import load_config_from_env

        config = load_config_from_env()

    if not os.path.exists(video_path):
        logger.warning("Input file %s does not exist. Skipping.", video_path)
        return

    logger.debug("video_path %s exists!", video_path)
    media_info = to_media_info(video_path)

    if isinstance(media_info, TVShowInfo) and is_known_tvshow(media_info.title):
        from .rename_media import tvshow_rename_media as task

        media_type = "tvshow"
    else:
        from .rename_media import movie_rename_media as task  # type: ignore

        media_type = "movie"

    destination_path = config[media_type]["destination"]
    logger.info("%s: mv %s %s", media_type, media_info.name, destination_path)
    task(video_path, destination_path=destination_path)
