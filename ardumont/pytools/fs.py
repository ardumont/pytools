# Copyright (C) 2019-2020  The ardumont-pytools developers
# See the AUTHORS file at the top-level directory of this distribution
# License: GNU General Public License version 3, or any later version
# See top-level LICENSE file for more information

import logging
import os
from sys import stdin
from typing import Iterator

import click

from ardumont.pytools import CONTEXT_SETTINGS

logger = logging.getLogger(__name__)


def read_from_stdin() -> Iterator[str]:
    """Read input from stdin, one string per line and returns the cleaned
       string as result.

    """
    for line in stdin:
        filepath = line.rstrip()
        if not filepath:
            continue
        if not os.path.exists(filepath):
            continue
        if filepath.startswith("./."):
            # relative path on empty files, skip
            continue
        if filepath.startswith(".") and not filepath.startswith("./"):
            # not relative path, skipping empty file
            continue
        yield filepath


@click.group(context_settings=CONTEXT_SETTINGS)
@click.pass_context
def directory(ctx):
    """Filesystem manipulation, especially renaming either audio directories
    or filenames."""
    ctx.ensure_object(dict)


@directory.command()
@click.pass_context
def audio(ctx):
    """Audio directory file renaming (replace "." by "-")

    The dummy radio car is only able to read folders with no . in the name...
    It was hard to troubleshoot...

    So this routine expects:
    - dirnames in stdin: `find -type d | tac | ...`
    - for each dirname
    - if hidden directory ("starting with a ."), skip it and continue
    - otherwise, check for a "." in its name
    - if not, skip it
    - if no "." in basename, skip it and continue
    - otherwise replace any "." by a "-"
    - if option dry run activated, log action and continue
    - otherwise actually rename the files

    """
    dry_run = ctx.obj.get("dry-run")

    for filepath in read_from_stdin():
        # manipulate only directories
        if not os.path.isdir(filepath):
            continue

        logger.debug("filepath: %s", filepath)
        _basename = os.path.basename(filepath)
        _dirpath = os.path.dirname(filepath)

        logger.debug("%s" % _basename)
        if _basename.startswith("."):  # hidden directory, skip
            logger.debug("Directory %s starts with '.', skipping", _dirpath)
            continue

        if "." not in _basename:  # nothing to do then, skip
            continue

        _new_basename = _basename.replace(".", "-")
        new_filepath = os.path.join(_dirpath, _new_basename)

        if os.path.exists(new_filepath):
            logger.debug("Path %s already exists, skipping" % new_filepath)
            continue

        logger.info("mv %s %s", filepath, new_filepath)

        if dry_run:
            continue
        os.rename(filepath, new_filepath)
