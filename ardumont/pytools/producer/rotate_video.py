# Copyright (C) 2018-2020  The ardumont-pytools developers
# See the AUTHORS file at the top-level directory of this distribution
# License: GNU General Public License version 3, or any later version
# See top-level LICENSE file for more information

import logging
import os

from ardumont.pytools.producer import PytoolsEventHandler
from ardumont.pytools.utils import is_video

logger = logging.getLogger(__name__)


EXTENSIONS_TO_CHECK = ["mkv", "mov", "avi", "mp4"]


class RotateVideoEventHandler(PytoolsEventHandler):
    """On closing file:
      - if video matching conversion pattern, send for rotation
      - else do nothing

    """

    def my_init(self, destination_path=None, cleanup=True):
        self.cleanup = cleanup
        self.destination_path = destination_path

    def _process_event(self, event):
        path = event.pathname
        vid = is_video(path, extensions_to_check=EXTENSIONS_TO_CHECK)
        if vid:  # check for video
            logger.info("Video %s scheduled for rotation" % path)
            rotation = os.path.basename(os.path.dirname(path))
            from ardumont.pytools.tasks.rotate_video import rotate_video_task

            rotate_video_task.send(
                path, rotation=rotation, destination_path=self.destination_path
            )
            return

        # here we are dealing with something that is not a movie
        # we might clean it up depending on self.cleanup
        if self.cleanup and os.path.exists(path):
            os.unlink(path)
