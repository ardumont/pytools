# Copyright (C) 2018-2020  The ardumont-pytools developers
# See the AUTHORS file at the top-level directory of this distribution
# License: GNU General Public License version 3, or any later version
# See top-level LICENSE file for more information

import logging

from ardumont.pytools.producer import PytoolsEventHandler

logger = logging.getLogger(__name__)


class RenamingEventHandler(PytoolsEventHandler):
    """On file closing or moving event, send to the renaming queue.

    """

    def my_init(self, destination_path: str, cleanup: bool = False):
        self.destination_path = destination_path
        self.cleanup = cleanup

    def _process_event(self, event):
        """Process event to rename the media path

        """
        path = event.pathname
        logger.info(f"Path {path} scheduled for renaming")
        from ardumont.pytools.tasks.rename_media import metadata_rename_media

        metadata_rename_media.send(
            path, destination_path=self.destination_path, cleanup=self.cleanup
        )
