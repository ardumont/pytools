# Copyright (C) 2018-2020  The ardumont-pytools developers
# See the AUTHORS file at the top-level directory of this distribution
# License: GNU General Public License version 3, or any later version
# See top-level LICENSE file for more information

from typing import List

from pyinotify import (
    IN_CLOSE_WRITE,
    IN_MOVED_TO,
    Notifier,
    ProcessEvent,
    WatchManager,
)


class PytoolsEventHandler(ProcessEvent):
    """Default pytools event handler for events:
      - closing a writable file
      - moved to a folder

    """

    def _process_event(self, event):
        """to override"""
        pass

    def process_IN_CLOSE_WRITE(self, event):
        self._process_event(event)

    def process_IN_MOVED_TO(self, event):
        self._process_event(event)


def exclude_path(path: str):
    """Exclude path whose name is . or path ends with .tmp

    """
    return path.startswith(".") or path.endswith(".tmp")


def make_notifier(
    handler: ProcessEvent, paths: List[str], recurse: bool = False
) -> Notifier:
    """Install a specific event notifier for IN_CLOSE_WRITE|IN_MOVED_TO events
       on paths. It's up to the caller to loop on the notifier's events.

    """
    wm = WatchManager(exclude_filter=exclude_path)
    mask = IN_CLOSE_WRITE | IN_MOVED_TO
    notifier = Notifier(wm, handler)
    for path in paths:
        wm.add_watch(path, mask, rec=recurse)
    return notifier
