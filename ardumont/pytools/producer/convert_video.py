# Copyright (C) 2018-2020  The ardumont-pytools developers
# See the AUTHORS file at the top-level directory of this distribution
# License: GNU General Public License version 3, or any later version
# See top-level LICENSE file for more information

import logging

from ardumont.pytools.producer import PytoolsEventHandler
from ardumont.pytools.utils import is_video

logger = logging.getLogger(__name__)


EXTENSIONS_TO_CHECK = ["mov", "3gp", "dv", "mkv", "mpg", "avi"]


class ConvertVideoEventHandler(PytoolsEventHandler):
    """On closing file:
      - if video matching conversion pattern, send for conversion
      - else do nothing

    """

    def my_init(self, keep_metadata):
        self.keep_metadata = keep_metadata

    def _process_event(self, event):
        path = event.pathname
        if is_video(path, extensions_to_check=EXTENSIONS_TO_CHECK):
            logger.info("Video %s scheduled for conversion" % path)
            from ardumont.pytools.tasks.convert_video import convert_video_task

            convert_video_task.send(path, keep_metadata=self.keep_metadata)


class ConvertEventHandler(PytoolsEventHandler):
    """On closing file:
      - if video matching conversion pattern, send for conversion
      - else send for media

    """

    def my_init(self, destination_path):
        self.destination_path = destination_path

    def _process_event(self, event):
        path = event.pathname
        if is_video(path, extensions_to_check=EXTENSIONS_TO_CHECK):
            logger.info("Video %s scheduled for conversion" % path)
            from ardumont.pytools.tasks.convert_video import convert_video_task

            convert_video_task.send(path)
        else:
            logger.info("Media %s scheduled for renaming" % path)
            from ardumont.pytools.tasks.basic_rename_media import basic_rename_media

            basic_rename_media.send(path, destination_path=self.destination_path)
