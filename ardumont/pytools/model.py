# Copyright (C) 2021  The ardumont-pytools developers
# See the AUTHORS file at the top-level directory of this distribution
# License: GNU General Public License version 3, or any later version
# See top-level LICENSE file for more information

import attr


@attr.s(frozen=True, slots=True)
class MediaInfo:
    path = attr.ib(type=str)
    name = attr.ib(type=str)
    title = attr.ib(type=str)
    extension = attr.ib(type=str)

    def sanitized_title(self):
        return self.title.replace("'", "-").replace(" ", "-").lower()


@attr.s(frozen=True, slots=True)
class TVShowInfo(MediaInfo):
    season = attr.ib(type=int)
    episode = attr.ib(type=int)

    def __str__(self):
        title = self.sanitized_title()
        season = self.season_str()
        episode = self.episode_str()
        return f"{title}-{season}{episode}.{self.extension}"

    def season_str(self):
        return f"s{self.season:0>2}"

    def episode_str(self):
        return f"e{self.episode:0>2}"


@attr.s(frozen=True, slots=True)
class MovieInfo(MediaInfo):
    year = attr.ib(type=int)

    def __str__(self):
        title = self.sanitized_title()
        return f"{title}-{self.year}.{self.extension}"
