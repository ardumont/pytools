# Copyright (C) 2018-2021  The ardumont-pytools developers
# See the AUTHORS file at the top-level directory of this distribution
# License: GNU General Public License version 3, or any later version
# See top-level LICENSE file for more information

from collections import OrderedDict
from functools import lru_cache
import json
import logging
import os
import re
from typing import Dict, Optional, Tuple

import click
from guessit import guessit
from tvmaze.api import Api, Show

from ardumont.pytools import CONTEXT_SETTINGS
from ardumont.pytools.fs import read_from_stdin
from ardumont.pytools.model import MediaInfo, MovieInfo, TVShowInfo
from ardumont.pytools.utils import sanitize, sanitize_string, yaml_export

tvmaze_instance = None


logger = logging.getLogger(__name__)


def to_media_info(filepath: str) -> MediaInfo:
    path = filepath.strip()
    guessed = guessit(path)
    filename = os.path.basename(path)
    if guessed["type"] == "episode":  # tvshow
        return TVShowInfo(
            path=filepath,
            name=filename,
            title=guessed["title"],
            season=int(guessed["season"]),
            episode=int(guessed["episode"]),
            extension=guessed["container"],
        )
    else:  # movie
        return MovieInfo(
            path=filepath,
            name=filename,
            title=guessed["title"],
            year=int(guessed["year"]),
            extension=guessed["container"],
        )


@lru_cache
def search_tvshow(tvmaze_instance: Api, tvshow: str) -> Optional[Show]:
    try:
        show = tvmaze_instance.show.get_by_name(tvshow)
    except Exception:
        show = None
    return show


@lru_cache
def make_tvmaze_instance() -> Api:
    return Api()


def is_known_tvshow(tvshow: Optional[str]) -> bool:
    """Check if the tvshow title is a known tvshow.

    Args:
        tvshow: Information extracted from tvmaze

    Returns:
        True if a tvshow, false otherwise

    """
    tvmaze_instance = make_tvmaze_instance()
    return bool(tvshow and search_tvshow(tvmaze_instance, tvshow))


def prepare_tvshow(show: Show) -> OrderedDict:
    """Given a show, extract a subset of information on it."""
    d = OrderedDict(name=show.name, tvmaze_id=show.id)

    for key in ["premiered", "genres", "language", "status"]:
        if hasattr(show, key):
            val = getattr(show, key)
            if val is not None:
                d[key] = val

    return d


@click.group(context_settings=CONTEXT_SETTINGS)
@click.pass_context
def tvshow(ctx):
    """tv-show manipulation"""
    pass


@tvshow.command()
@click.argument("tvshow")
@click.pass_context
def search(ctx, tvshow: str):
    """ "Search information on a given tv-show"""
    show = search_tvshow(make_tvmaze_instance(), tvshow)
    if show:
        result = yaml_export(prepare_tvshow(show))
    else:
        result = "No result found"
    click.echo(result)


def prepare_tvshow_fs(
    root_path: str, tvshow: str, tvshow_id: int, force: bool = False
) -> None:
    """Prepare tvshow on the filesystem."""
    title = sanitize_string(tvshow)
    new_path = f"{root_path}/{title[0]}/{title}"

    os.makedirs(new_path, exist_ok=True)

    new_tvshow_path = f"{new_path}/.tvshow-id"

    if not force and os.path.exists(new_tvshow_path):
        existing_tvshow_id = open(new_tvshow_path).read().strip()

        if int(existing_tvshow_id) == tvshow_id:
            # silently ignore as the result is the same
            return

        raise ValueError(f"Conflict: Existing entry {existing_tvshow_id} for {tvshow}")

    with open(new_tvshow_path, "w") as f:
        f.write(f"{tvshow_id}\n")
    logger.info("tv-show id %s configured %s", str(tvshow_id).zfill(5), new_path)


@tvshow.command()
@click.argument("tvshow")
@click.option(
    "-t", "--tvmaze-id", help="tvmaze id to set for the tv show", required=True
)
@click.option(
    "-p",
    "--root-path",
    help="tvshow root path, default to local directory",
    default=".",
)
@click.option(
    "--force/--no-force", help="If conflict appears, overwrite", default=False
)
@click.pass_context
def init(ctx, tvshow: str, tvmaze_id: int, root_path: str, force: bool):
    """Create a tvshow folder with the necessary setup.

    By default, this raises a warning in case a setup already exist for the
    tvshow. To force an update on conflicting entries, use the --force flag.

    """
    prepare_tvshow_fs(root_path, tvshow, tvmaze_id, force)


@tvshow.command("init-from-json")
@click.option(
    "-m",
    "--media-json",
    help="the media.json to update with a new entry",
    default="./media.json",
)
@click.option(
    "-p",
    "--root-path",
    help="the root folder for the shows folder creation ",
    default=".",
)
@click.option(
    "--force/--no-force", help="If conflict appears, overwrite", default=False
)
@click.pass_context
def init_from_json(ctx, media_json: str, root_path: str, force: bool):
    """Initialization tvshow folders from a media.json

    By default, this raises a warning in case a setup already exist for the
    tvshow. To force an update on conflicting entries, use the --force flag.

    """
    if not os.path.exists(media_json):
        raise ValueError("Please provide a media.json file.")
    with open(media_json, "r") as f:
        media = json.loads(f.read())

    invalid_entries = []
    for title, show in media.items():
        assert show
        assert title
        tvshow = show.get("short")
        tvmaze_id = show.get("tvmaze-id")

        if tvshow and tvmaze_id:
            prepare_tvshow_fs(root_path, tvshow, tvmaze_id, force=force)
        else:
            invalid_entries.append(title)

    if invalid_entries:
        print(f"Invalid entries found: {', '.join(invalid_entries)}")


@tvshow.command()
@click.argument("tvshow")
@click.option(
    "-t", "--tvmaze-id", help="tvmaze id to set for the tv show", required=True
)
@click.option(
    "-m",
    "--media-json",
    help="the media.json to update with a new entry",
    default="./media.json",
)
@click.option(
    "--force/--no-force", help="If conflict appears, overwrite", default=False
)
@click.pass_context
def update(ctx, tvshow: str, tvmaze_id: int, media_json: str, force: bool):
    """Update a media.json file with a new tvshow entry:

    Media.json format:
    {
    "Normalized Title": {
        "short": "normalized-title",
        "tvmaze-id": 343
    },
    ...
    }

    """
    media: Dict = {}
    if os.path.exists(media_json):
        with open(media_json, "r") as f:
            media = json.loads(f.read())

    existing_tvshow = media.get(tvshow)
    if existing_tvshow and not force:
        existing_tvmaze_id = existing_tvshow["tvmaze-id"]
        if existing_tvmaze_id == tvmaze_id:
            # silently ignore conflict as it's the same value anyway
            click.echo(json.dumps(media))
            return

        raise ValueError("Conflict: Existing entry %s for %s", tvshow, existing_tvshow)

    media.update(
        {
            tvshow: {
                "short": sanitize_string(tvshow),
                "tvmaze-id": tvmaze_id,
            }
        }
    )

    click.echo(json.dumps(media))


@click.group(context_settings=CONTEXT_SETTINGS)
@click.pass_context
def movie(ctx):
    """Movie manipulation, mainly cleanup routines."""
    ctx.ensure_object(dict)


@movie.command("sanitize")
@click.argument("movie_path")
@click.pass_context
def sane(ctx, movie_path: str):
    """Rename movie filename appropriately"""
    dry_run = ctx.obj.get("dry-run")
    new_movie_path = sanitize(movie_path)
    logger.info("mv %s %s", movie_path, new_movie_path)
    if dry_run:
        return
    os.rename(movie_path, new_movie_path)


PATTERN = re.compile(
    r"[a-zA-Z0-9-_'\" ]*(?P<year>[0-9]{4}).[a-z-A-Z0.9]*", flags=re.DOTALL
)


@movie.command()
@click.pass_context
def sort(ctx):
    """Sort movies by their release date year.

    Expected input filenames: <title>-<year>.<ext>
    Expected output filenames: <rounded-year>/<title>-<year>.<ext>

    """
    dry_run = ctx.obj.get("dry-run")
    for filepath in read_from_stdin():
        # manipulate only files
        if os.path.isdir(filepath):
            continue

        filename = os.path.basename(filepath)
        if filename.startswith("."):
            # do not manipulate hidden file
            continue

        if "." not in filename:
            # we need an extension
            continue

        logger.debug("filepath: %s", filepath)
        logger.debug("filename: %s", filename)

        res = PATTERN.match(filename)
        if not res:
            # did not find the right pattern, do not touch
            continue

        groups = res.groupdict()
        logger.debug("Groups: %s", groups)
        year = int(groups["year"])
        logger.debug("filename: %s ; year: %s", filename, year)

        new_parent_foldername = year - (year % 10)
        new_parent_folderpath = os.path.join(
            os.path.dirname(filepath), str(new_parent_foldername)
        )
        logger.debug("new dirpath: %s", new_parent_folderpath)
        new_path = os.path.join(new_parent_folderpath, filename)
        logger.info("mv %s %s", filepath, new_path)

        if dry_run:
            continue
        os.makedirs(new_parent_folderpath, exist_ok=True)
        os.rename(filepath, new_path)
