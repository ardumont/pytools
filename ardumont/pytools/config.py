# Copyright (C) 2015-2020  The SWH and ardumont-pytools developers
# See the AUTHORS file at the top-level directory of this distribution
# License: GNU General Public License version 3, or any later version
# See top-level LICENSE file for more information

import os
from typing import Any, Mapping

import yaml


def exists_accessible(path: str) -> bool:
    """Check whether a path exists, and is accessible.

    Returns:
        True if the path exists and is accessible
        False if the path does not exist

    Raises:
        PermissionError if the path cannot be read.
    """

    try:
        os.stat(path)
    except PermissionError:
        raise
    except FileNotFoundError:
        return False
    else:
        if os.access(path, os.R_OK):
            return True
        raise PermissionError("Permission denied: %r" % path)


def load_config(path: str) -> Mapping[str, Any]:
    """Load the config named `path` from the Software Heritage
       configuration paths.

    """
    if exists_accessible(path):
        with open(path) as f:
            return yaml.safe_load(f)

    return {}
