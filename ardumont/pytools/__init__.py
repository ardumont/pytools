# Copyright (C) 2019-2020  The SWH and ardumont-pytools developers
# See the AUTHORS file at the top-level directory of this distribution
# License: GNU General Public License version 3, or any later version
# See top-level LICENSE file for more information

import logging

import click
import pkg_resources

_LOG_LEVELS = [s.lower() for s in logging._nameToLevel.keys()]


CONTEXT_SETTINGS = dict(help_option_names=["-h", "--help"])


logger = logging.getLogger(__name__)


@click.group(context_settings=CONTEXT_SETTINGS)
@click.option(
    "--log-level",
    "-l",
    default="info",
    type=click.Choice(_LOG_LEVELS),
    help="Log level (default to info)",
)
@click.option("--dry-run", is_flag=True, default=False)
@click.pass_context
def cli(ctx, log_level, dry_run):
    logging.basicConfig(
        level=log_level.upper(),
        format="%(asctime)s %(levelname)s %(name)s %(message)s",
    )

    ctx.ensure_object(dict)
    ctx.obj["dry-run"] = dry_run
    if dry_run:
        logger.info("** DRY-RUN **")


def main():
    # load plugins that define cli sub commands
    for entry_point in pkg_resources.iter_entry_points("pytools.cli.subcommands"):
        try:
            cmd = entry_point.load()
            cli.add_command(cmd, name=entry_point.name)
        except Exception as e:
            logger.warning("Could not load subcommand %s: %s", entry_point.name, e)

    return cli(auto_envvar_prefix="PYTOOLS")


if __name__ == "__main__":
    main()
