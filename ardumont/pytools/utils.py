# Copyright (C) 2016-2021  The ardumont-pytools developers
# See the AUTHORS file at the top-level directory of this distribution
# License: GNU General Public License version 3, or any later version
# See top-level LICENSE file for more information

from collections import OrderedDict
import hashlib
import logging
import os
import re
from typing import Any, List, Mapping, Optional

import yaml

from ardumont.pytools.command import exec_commands
from ardumont.pytools.date import format_date, to_date
from ardumont.pytools.metadata import extract_date

logger = logging.getLogger(__name__)


def rename(oldpath, newpath):
    """Rename a oldpath to newpath.

    If newpath already exists, adds a sequence to the name and try
    again.

    """
    if not os.path.exists(newpath):
        parent_dir = os.path.dirname(newpath)
        os.makedirs(parent_dir, exist_ok=True)
        os.rename(oldpath, newpath)
        return newpath
    new_filename_prefix = newpath.split(".")[0]
    new_filename_ext = os.path.splitext(newpath)[-1]
    seq = 1
    s = "%02d" % seq

    newpath = f"{new_filename_prefix}.{s}{new_filename_ext}"
    while os.path.exists(newpath):
        seq += 1
        s = "%02d" % seq
        newpath = f"{new_filename_prefix}.{s}{new_filename_ext}"

    os.rename(oldpath, newpath)
    return newpath


VIDEO_EXT = ["mp4"]


def is_video(path, extensions_to_check=VIDEO_EXT):
    """Check if a given path matches a video file. The check is done through
    extensions.

    """
    exts = os.path.splitext(path.lower())
    extension = exts[1]
    for video_ext in extensions_to_check:
        if video_ext in extension:
            return True
    return False


PRE_SANITIZE_CHARS: List[str] = ["[", "]", "{", "}", "(", ")"]


def pre_sanitize_string(s: str) -> str:
    """Remove englobing noisy characters from the filename."""
    s = s.lower()
    return "".join(c for c in s if c not in PRE_SANITIZE_CHARS)


def sanitize_string(s: str) -> str:
    """Remove noisy characters from the filename."""
    s = s.lower()
    if "_" in s:
        s = s.replace("_", "-")
    if " " in s:
        s = s.replace(" ", "-")
    if "." in s:
        s = s.replace(".", "-")
    if "--" in s:
        s = s.replace("--", "-")
    return s


PATTERN_MOVIE = re.compile(
    r"(?P<filename>[a-zA-Z0-9-_'\" ]+?)(?P<year>[0-9]{4}){1}?.*", re.DOTALL
)

PATTERN_TVSHOW = re.compile(
    r"(?P<filename>[a-zA-Z0-9-_'\" ]+?)(?P<season>s[0-9]{1,2}){1}(?P<episode>e[0-9]{1,2}){0,1}.*",  # noqa
    re.DOTALL,
)


def sanitize(path: str) -> str:
    """Sanitize path to a media to something more compliant.

    Replaces `_`, ` ` or `.` by `-` on the prefix filename (not the extension)

    This does not touch the filesystem.

    """
    dirname = os.path.dirname(path)
    filename = pre_sanitize_string(os.path.basename(path))
    assert "." in filename, f"{filename} should have a ."
    terms = filename.split(".")
    ext = terms[-1]
    prefix_filename = "-".join(terms[:-1])
    # check for tvshow
    res = PATTERN_TVSHOW.match(prefix_filename)
    if res:  # tvshow detected
        groups = res.groupdict()
        logger.debug("Groups: %s", groups)
        season = groups["season"]
        episode = groups.get("episode", "")
        if not episode:
            episode = ""
        filename = sanitize_string(f"{groups['filename']}-{season}{episode}")
    else:
        res = PATTERN_MOVIE.match(prefix_filename)  # check for movie
        if not res:
            # unrecognised, fail
            raise ValueError(
                f"{filename} should have either a 'year' or a 'season' pattern"
            )
        groups = res.groupdict()
        logger.debug("Groups: %s", groups)
        year = int(groups["year"])
        filename = sanitize_string(f"{groups['filename']}-{year}")
    return os.path.join(dirname, f"{filename}.{ext}")


def read_tvshow_id(tvtool_tvshow_config: str) -> Optional[int]:
    """Read tvshow identifier from a file"""
    if not os.path.exists(tvtool_tvshow_config):
        logger.warning(
            "tvshow configuration file %s not found, stopping.", tvtool_tvshow_config
        )
        return None
    with open(tvtool_tvshow_config, "r") as f:
        tvshow_id = f.read()
    try:
        return int(tvshow_id)
    except ValueError:
        logger.warning(
            "tshow configuration file %s should contain an id: %s",
            tvtool_tvshow_config,
            tvshow_id,
        )
        return None


def _tvshow_rename_cmds(
    new_path: str, config: Mapping[str, Any] = {}
) -> List[List[str]]:
    """Trigger a tvshow rename run at new_path.

    Expects to find a configuration file holding the tvshow id at the top of the
    specific tvshow folder. If this is not found, no command is returned (failure) with
    a warning log message.

    Otherwise, returns the command to actually execute.

    """
    if not config:
        from ardumont.pytools.dramatiq import load_config_from_env

        config = load_config_from_env()

    cfg = config["application-command"]
    app_global_cfg = cfg["config-path"]
    app_cmd = cfg["command"]
    tvshow_id_filename = cfg["tvshow-id-filename"]

    tvshow_id = None
    for rel_path in ["..", "."]:
        if tvshow_id:
            break
        tvtool_tvshow_config = os.path.join(new_path, rel_path, tvshow_id_filename)
        tvshow_id = read_tvshow_id(tvtool_tvshow_config)

    cmd = [
        app_cmd,
        f"--config-path={app_global_cfg}",
    ]
    if tvshow_id:
        cmd.append(f"--id-tvmaze={tvshow_id}")

    cmd.append(new_path)

    return [cmd]


def tvshow_rename(new_path: str, config: Mapping[str, Any] = {}) -> bool:
    """Trigger tvshow rename command on new_path."""
    cmds = _tvshow_rename_cmds(new_path, config)
    return exec_commands(cmds) if cmds else False


def _movie_rename_cmds(
    new_path: str, config: Mapping[str, Any] = {}
) -> List[List[str]]:
    """Trigger a tvshow rename run at new_path.

    Otherwise, returns the command to actually execute.

    """
    if not config:
        from ardumont.pytools.dramatiq import load_config_from_env

        config = load_config_from_env()

    cfg = config["application-command"]
    app_global_cfg = cfg["config-path"]
    app_cmd = cfg["command"]

    cmd = [
        app_cmd,
        f"--config-path={app_global_cfg}",
        new_path,
    ]
    return [cmd]


def movie_rename(new_path: str, config: Mapping[str, Any] = {}) -> bool:
    """Trigger movie rename command on new_path."""
    cmds = _movie_rename_cmds(new_path, config)
    return exec_commands(cmds) if cmds else False


def sha512_path(path: str) -> str:
    with open(path, "rb") as f:
        hashes = hashlib.sha512()
        for chunk in f:
            hashes.update(chunk)
    return hashes.hexdigest()


def check_for_hash_clash(src_path: str, dest_path: str) -> bool:
    """Check src_path and dest_path are clashing or not."""
    h_dest = sha512_path(dest_path)
    h_src = sha512_path(src_path)
    return h_src == h_dest


def yaml_export(data):
    """Export into yaml, adding support for ordered dict"""

    def ordered_dict_representer(self, value):
        return self.represent_mapping("tag:yaml.org,2002:map", value.items())

    yaml.add_representer(OrderedDict, ordered_dict_representer)

    return yaml.dump(data)


def compute_destination_path(src_path: str, destination_path: str) -> Optional[str]:
    """Compute the destination path out of the src path"""
    date = to_date(extract_date(src_path))
    if not date:
        return None
    year = str(date.year)
    month = "%02d" % date.month
    parent_dir = os.path.join(destination_path, year, month)
    sdate = format_date(date)
    assert sdate is not None
    dest_path_prefix = os.path.join(parent_dir, sdate)
    split_src_path = os.path.splitext(src_path)
    dest_path_ext = split_src_path[-1].lower()
    return f"{dest_path_prefix}{dest_path_ext}"


def copy_or_rename_with_clash(src_path: str, dest_path: str, cleanup: bool = False):
    """Copy/Rename with policy clash"""
    parent_dir = os.path.dirname(dest_path)
    os.makedirs(parent_dir, exist_ok=True)

    if os.path.exists(dest_path):
        # Name clash, we need to rename with some smart policy
        # need to check if it's the same file
        if check_for_hash_clash(src_path, dest_path):  # File are the same
            if cleanup:
                logger.warning("%s already exists! Removing %s", dest_path, src_path)
                # It is, we remove src_path as the copy already took place
                os.unlink(src_path)
            else:
                logger.warning("%s already exists! Skipping %s", dest_path, src_path)
            return

        # Files are named the same but have different contents
        # We want that file which has a different hash
        # Need to compute an unclashed name
        i = 0
        dest_path_prefix, dest_path_ext = dest_path.split(".")
        while True:
            i += 1
            idx = "%02d" % i
            dest_path = f"{dest_path_prefix}-{idx}.{dest_path_ext}"
            if not os.path.exists(dest_path):
                break
            if check_for_hash_clash(src_path, dest_path):
                if cleanup:
                    logger.warning(
                        "%s already exists! Removing %s", dest_path, src_path
                    )
                    # It is, remove src_path as the copy already took place
                    os.unlink(src_path)
                else:
                    logger.warning(
                        "%s already exists! Skipping %s", dest_path, src_path
                    )
                    return

    if cleanup:
        logger.info("mv %s %s", src_path, dest_path)
        os.rename(src_path, dest_path)
    else:
        logger.info("cp %s %s", src_path, dest_path)
        with open(dest_path, "wb") as _out:
            with open(src_path, "rb") as _in:
                for chunk in _in:
                    _out.write(chunk)
