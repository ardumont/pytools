# Copyright (C) 2019-2020  The SWH and ardumont-pytools developers
# See the AUTHORS file at the top-level directory of this distribution
# License: GNU General Public License version 3, or any later version
# See top-level LICENSE file for more information

import logging
import os
import sys

import click

from ardumont.pytools import CONTEXT_SETTINGS
from ardumont.pytools.dramatiq import load_broker_from_env
from ardumont.pytools.metadata import clean as metadata_clean
from ardumont.pytools.producer import make_notifier

logger = logging.getLogger(__name__)


@click.group(context_settings=CONTEXT_SETTINGS)
@click.pass_context
def task(ctx):
    """Send task event for consumption queues

    """
    pass


@task.command()
@click.pass_context
def clean_metadata(ctx):
    """Clean metadata from video files passed as input (specifically mp4).

    """
    dry_run = ctx.obj["dry-run"]
    for line in sys.stdin:
        video_path = line.rstrip()
        logger.info("Stripping metatada on %s", video_path)
        if dry_run:
            continue
        metadata_clean.send(video_path)


@task.command()
@click.pass_context
def convert_video(ctx):
    """Send video paths from stdin to the convert video queue.

    """
    load_broker_from_env()
    dry_run = ctx.obj["dry-run"]
    from ardumont.pytools.tasks.convert_video import convert_video_task as task

    for line in sys.stdin:
        video_path = line.strip()
        logger.info(video_path)
        if dry_run:
            continue
        task.send(video_path)


@task.command()
@click.pass_context
def sort_media(ctx):
    """Send video paths from stdin to the sorting media queue

    """
    dry_run = ctx.obj["dry-run"]
    load_broker_from_env()
    from ardumont.pytools.tasks.sort_media import sort_media_task as task

    for line in sys.stdin:
        video_path = line.strip()
        logger.info(video_path)
        if dry_run:
            continue
        task.send(video_path)


@task.command()
@click.option(
    "--destination-path",
    default="/volume/share/pictures",
    help="destination path to move files.",
)
@click.option(
    "--cleanup",
    is_flag=True,
    default=False,
    help="By default, do not clean up the source.",
)
@click.pass_context
def rename_media(ctx, destination_path, cleanup):
    """Send video paths from stdin to the renaming queue.

    """
    dry_run = ctx.obj["dry-run"]
    load_broker_from_env()
    from ardumont.pytools.tasks.rename_media import (  # noqa
        metadata_rename_media as task,
    )

    for line in sys.stdin:
        media_path = line.strip()
        logger.info(media_path)
        if dry_run:
            continue
        task.send(
            media_path,
            destination_path=destination_path,
            cleanup=cleanup,
            dry_run=dry_run,
        )


@task.command()
@click.pass_context
def rotate_video(ctx):
    """Send videos paths from stding to the rotate queue.

    """
    dry_run = ctx.obj["dry-run"]
    load_broker_from_env()
    from ardumont.pytools.tasks.rotate_video import rotate_video_task as task

    for line in sys.stdin:
        line = line.strip()
        rotation, video_path = line.split(" ")
        logger.info(f"Rotate path {video_path} with rotation {rotation}")
        if dry_run:
            continue
        task.send(video_path, rotation=rotation)


@task.command()
@click.pass_context
@click.option("--name", "-n", default="world")
def hello(ctx, name):
    """Send a simple hello event to queue

    """
    load_broker_from_env()
    from ardumont.pytools.tasks.hello import hello_task as task

    task.send(name)


@click.group(context_settings=CONTEXT_SETTINGS)
@click.pass_context
def watch(ctx):
    """Install event watcher on directories.

    """
    load_broker_from_env()


@watch.command()
@click.option(
    "--path",
    multiple=True,
    default=["/volume/share/camera/input/"],
    help="Default path to watch",
)
@click.option(
    "--destination-path",
    default="/volume/share/camera",
    help="Default path to send renaming media",
)
@click.option("--video", is_flag=True, default=False, help="Watch for video")
@click.option(
    "--recurse",
    is_flag=True,
    default=False,
    help="Listen to all folders beneath the root folder",
)
@click.option(
    "--keep-metadata/--no-keep-metadata",
    is_flag=True,
    default=True,
    help="Keep (or do not) metadata during video conversion.",
)
def convert_video_event(path, destination_path, video, recurse, keep_metadata):
    """Send convert video event messages on registered path(s) notification

    """
    paths = path
    for path in paths:
        if not os.path.exists(path):
            os.makedirs(path, exist_ok=True)

    logger.info("Watching events on %s" % (", ".join(paths),))

    if video:
        from ardumont.pytools.producer.convert_video import (  # noqa
            ConvertVideoEventHandler,
        )

        handler = ConvertVideoEventHandler(keep_metadata=keep_metadata)
    else:
        from ardumont.pytools.producer.convert_video import ConvertEventHandler

        handler = ConvertEventHandler(destination_path=destination_path)

    # Listen to events and send new path for
    notifier = make_notifier(handler, paths, recurse=recurse)
    notifier.loop()


@watch.command()
@click.option(
    "--path",
    multiple=True,
    default=["/volume/share/camera"],
    help="Default path to watch",
)
@click.option(
    "--destination-path",
    default="/volume/share/pictures",
    help="destination path to move files.",
)
@click.option(
    "--cleanup",
    is_flag=True,
    default=False,
    help="By default, do not clean up the source.",
)
def rename_media_event(path, destination_path, cleanup):
    """Send rename media event message on registered path(s) notification

    """
    paths = path
    for path in paths:
        if not os.path.exists(path):
            os.makedirs(path, exist_ok=True)

    logger.info("Watching events on %s" % (", ".join(paths),))

    from ardumont.pytools.producer.rename_media import RenamingEventHandler

    handler = RenamingEventHandler(destination_path=destination_path, cleanup=cleanup)
    notifier = make_notifier(handler, paths)
    notifier.loop()


@watch.command()
@click.option(
    "--path",
    multiple=True,
    default=["/tmp/rotate/90", "/tmp/rotate/180", "/tmp/rotate/270"],
    help="Default path to watch",
)
@click.option(
    "--destination-path",
    default="/volume/share/camera",
    help="Destination path to move the file after the rotation",
)
def rotate_video_event(path, destination_path):
    """Send rotate video event messages on registered path(s) notification

    """
    paths = path
    for path in paths:
        if not os.path.exists(path):
            os.makedirs(path, exist_ok=True)

    logger.info("Watching events on %s" % (", ".join(paths),))

    from ardumont.pytools.producer.rotate_video import RotateVideoEventHandler

    handler = RotateVideoEventHandler(destination_path=destination_path)
    # Listen to events and send new path for
    notifier = make_notifier(handler, paths)
    notifier.loop()
