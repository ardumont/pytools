# Copyright (C) 2019-2020  The ardumont-pytools developers
# See the AUTHORS file at the top-level directory of this distribution
# License: GNU General Public License version 3, or any later version
# See top-level LICENSE file for more information

# install PyExifinfo
# sudo apt-get install libimage-exiftool-perl
# sudo pip3 install PyExifinfo

from datetime import datetime
import logging
import os
from typing import Any, Dict, List, Optional, Union

from mutagen.mp4 import MP4
import pyexifinfo as p

from ardumont.pytools.date import parse_date_from_filename, to_date

logger = logging.getLogger(__name__)


def extract_metadata(filepath: str) -> Dict[str, Any]:
    """Try to extract metadata information from filepath.

    """
    try:
        data = p.get_json(filepath)
        if not data and not data[0]:
            return {}

        data = data[0]
        if isinstance(data, str):
            return {}
        return data
    except Exception:
        return {}


def _extract_date(
    data_or_path: Union[str, Dict], fields: List[str]
) -> Optional[datetime]:
    if isinstance(data_or_path, str):  # path
        data = extract_metadata(data_or_path)
        if not data:
            return None
    else:
        data = data_or_path

    dates: List[datetime] = [
        d for d in map(lambda f: to_date(data.get(f)), fields) if d
    ]
    return min(dates) if dates else None


def date_file(data: Union[str, Dict]) -> Optional[datetime]:
    """Extract date information from the file's modify or access date
       time, whichever is the minimum.

    Args:
        data: Either a metadata dict or a path (file)

    Returns
        the extracted date
    """
    return _extract_date(data, fields=["File:FileModifyDate", "File:FileAccessDate",])


def date_from_metadata(data: Union[str, Dict]) -> Optional[datetime]:
    """Given a data, try and extract the images date, whichever is the
       minimum date.

    Args:
        data: Either a metadata dict or a path (file)

    Returns
        the extracted date

    """
    return _extract_date(
        data,
        fields=[
            "Image:DateTimeOriginal",
            "EXIF:DateTimeOriginal",
            "Composite:GPSDateTime",
            "File:FileModifyDate",
            "File:FileAccessDate",
            "EXIF:DateTimeDigitized",
            "EXIF:DateTime",
            "EXIF:CreateDate",
            "QuickTime:CreateDate",
            "QuickTime:MediaCreateDate",
        ],
    )


def extract_date(filepath: str) -> Optional[datetime]:
    """Extract date from the metadata. If none is found, extract from the
    filename.

    """
    _date = date_from_metadata(filepath)
    if _date:
        return _date

    filename = os.path.basename(filepath)
    return parse_date_from_filename(filename)


def clean(video_path):
    """Clean metadata on video_path if it's an mp4 file

    """
    logger.debug("path: %s", video_path)
    exts = os.path.splitext(video_path.lower())
    extension = exts[1]
    logger.debug("extension: %s", extension)
    if extension == ".mp4":
        logger.debug("stripping mp4 %s", video_path)
        v = MP4(video_path)
        logger.debug("Metadata: %s", v)
        v.clear()
        logger.debug("Metadata: %s once cleaned", v)
        v.save()
        logger.debug("Saving stripping data on %s", video_path)

    return video_path
